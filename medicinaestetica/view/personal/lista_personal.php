<script type="text/javascript">
	$(document).ready(function()
  {
    var upload_foto_base64=false;
    var upload_foto_file=false;

		$("#b_personal").keyup(function()
		{			
			$.ajax({
			   	type: 'post',
			   	url: '../controller/personal/buscar_personal.php',
				  data: 'nombre='+$(this).val(),
			   	success: function(html)
			   	{    					
					$("#lista_pacientes").html(html);
			   	},
			    beforeSend:function()
			   {									
					$("#lista_pacientes").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...');
			   }
			  });
		});


$("#s_camara").click(function()
{
    $("#foto").hide();
    $("#btns_control_camara").show();
    $("#camara").show();


    $(this).hide();
    $("#s_file").hide();
    $("#botonFile").hide();
    $("#botonFoto").show();

    $("#uploadPreview").hide();

    navigator.getUserMedia({
        'audio': false,
        'video': true
    }, function(streamVideo) {
        datosVideo.StreamVideo = streamVideo;
        datosVideo.url = window.URL.createObjectURL(streamVideo);
        jQuery('#camara').attr('src', datosVideo.url);

    }, function() 
    {
        alert('No fue posible obtener acceso a la cámara.');
    });

});

$("#s_file").click(function()
{
    $("#btns_control_camara").show();
    $(this).hide();
    $("#s_camara").hide();
    $("#botonFoto").hide();
    $("#botonFile").show();

     $("#uploadPreview").show();
     $("#foto").hide();

});

window.URL = window.URL || window.webkitURL;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia ||
function() 
{
    alert('Su navegador no soporta navigator.getUserMedia().');
};

//Este objeto guardará algunos datos sobre la cámara
window.datosVideo = 
{
    'StreamVideo': null,
    'url': null
}

/*jQuery('#botonIniciar').on('click', function(e) 
{

    //Pedimos al navegador que nos da acceso a 
    //algún dispositivo de video (la webcam)
    navigator.getUserMedia({
        'audio': false,
        'video': true
    }, function(streamVideo) {
        datosVideo.StreamVideo = streamVideo;
        datosVideo.url = window.URL.createObjectURL(streamVideo);
        jQuery('#camara').attr('src', datosVideo.url);

    }, function() 
    {
        alert('No fue posible obtener acceso a la cámara.');
    });

});*/





/*jQuery('#botonDetener').on('click', function(e) {

    if (datosVideo.StreamVideo) {
        datosVideo.StreamVideo.stop();
        window.URL.revokeObjectURL(datosVideo.url);
    }

});*/

jQuery('#botonFoto').on('click', function(e) {
    var oCamara, oFoto, oContexto, w, h;

    oCamara = jQuery('#camara');
    oFoto = jQuery('#foto');
    w = oCamara.width();
    h = oCamara.height();
    oFoto.attr({
        'width': w,
        'height': h
    });
    oContexto = oFoto[0].getContext('2d');
    oContexto.drawImage(oCamara[0], 0, 0, w, h);    

    $("#camara").hide();
    $("#s_camara").show();
    $("#s_file").show();
    $(this).hide();

     if (datosVideo.StreamVideo) 
     {
        //datosVideo.StreamVideo.stop();
        window.URL.revokeObjectURL(datosVideo.url);
    }

    $("#camara").hide();
    $("#foto").show();

    upload_foto_base64=true;
    upload_foto_file=false;
    
    //window.location.href = dataURL;
  });


    jQuery('#botonFile').on('click', function(e) 
    {
      var oCamara, oFoto, oContexto, w, h;
      $("#file_img").click();        

    });

    $( "#file_img" ).change(function(e) 
    {
      
      //alert("Preeview Img");

      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("file_img").files[0]);

      oFReader.onload = function (oFREvent) 
        {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };


      $("#botonFoto").hide();
      $("#botonFile").hide();

      $("#s_camara").show();
      $("#s_file").show();


      upload_foto_base64=false;
      upload_foto_file=true;  

    });


    $("#nuevo_personal_form").validate({
        rules: {
            nombre: {
                minlength: 2,
                required: true                
            },
            ap_paterno: {
                minlength: 2,
                required: true
            },
            ap_materno: {
                minlength: 2,
                required: true
            }
            ,
            edad: {
                minlength: 1,
                required: true
            },
            estado: {
                minlength: 2,
                required: true                
            },
            ciudad: {
                minlength: 2,
                required: true
            },
            colonia: {
                minlength: 2,
                required: true
            }
            ,
            calle: {
                minlength: 1,
                required: true
            },
            numero_calle: {                
                required: true
            },
            telefono_m: {                
                required: true
            },            
            telefono_p: {                
                required: true
            }
        },
        messages: {
                    nombre: {
                        required: "Nombre Requerido.",
                        minlength: "Nombre Mayor a 2 Caracteres."
                    },
                    ap_paterno: {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    ap_materno: {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    edad: {
                        required: "Edad Requerido.",
                        minlength: "Edad Mayor a 0."
                    },
                    estado: {
                        required: "Estado Requerido.",
                        minlength: "Mayor a 2 Caracteres."              
                    },
                    ciudad: {
                        required: "Ciudad Requerido.",
                        minlength: "Mayor a 2 Caracteres."  
                    },
                    colonia: {
                        required: "Colonia Requerido.",
                        minlength: "Mayor a 2 Caracteres."  
                    }
                    ,
                    calle: {
                        required: "Calle Requerido.",
                        minlength: "Mayor a 2 Caracteres."  
                    },
                    numero_calle: {
                        required: "# Requerido."
                    },
                    telefono_m: {
                         required: "Telefono Requerido."
                    },                    
                    telefono_p: {
                         required: "Telefono Requerido."
                    }

                  },
        highlight: function (element) 
        {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) 
        {
            element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) 
        {
            // do other things for a valid form
            //varificar de que fuente se optiene la imagen
          if(upload_foto_file==true || upload_foto_base64==true)
          {                       
                                  
                        $.ajax({
                            type: 'post',
                            url: '../controller/personal/nuevo_personal.php',
                            data: $('#nuevo_personal_form').serialize(),
                            success: function(html)
                            {   
                                //subir foto  
                                var id_persona=html;

                                if(upload_foto_base64==true)//subir img por String base64img
                                {     

                                          //enviar img de canvas source
                                          var canvas = document.getElementById('foto');  
                                          var dataURL = canvas.toDataURL();  
                                        
                                          $.ajax({
                                                  type: 'post',
                                                  url: '../controller/personal/upload_foto_base64img.php',
                                                  data: {foto:dataURL,id_persona:id_persona},
                                                  success: function(html)
                                                  {   
                                                      $("#rs_registro").html("");  
                                                      //limpiar form 
                                                      //cerrar form
                                                      //$('#modal_nuevo_paciente').modal('hide');  
                                                      upload_foto_base64=false;
                                                      upload_foto_file=false;
                                                      $('#nuevo_personal_form').trigger("reset");

                                                       $("#uploadPreview").attr("src","../recursos/img/user_nuevo.png");
                                                       $("#uploadPreview").show();
                                                       $("#foto").hide();
                                                       $("#camara").hide();
                                                  },
                                                  beforeSend:function()
                                                  {                  
                                                    
                                                  }
                                                });
                                             
                                }else
                                  {
                                    if(upload_foto_file==true)//subir 
                                    {
                                              
                                              var file_data = $('#file_img').prop('files')[0];   
                                              var form_data = new FormData();                  
                                              form_data.append('file_img', file_data);
                                              form_data.append('id_persona', id_persona);
                                              
                                              $.ajax({
                                              url: "../controller/personal/upload_foto_file.php",
                                              type: "POST",
                                              data:  form_data,
                                              contentType: false,
                                              cache: false,
                                              processData:false,
                                              success: function(data)
                                              {                                                
                                                $("#rs_registro").html("");  
                                                 //$('#modal_nuevo_paciente').modal('hide');  
                                                 upload_foto_base64=false;
                                                 upload_foto_file=false;
                                                 $('#nuevo_personal_form').trigger("reset");
                                                
                                                 $("#uploadPreview").attr("src","../recursos/img/user_nuevo.png");
                                                 $("#uploadPreview").show();
                                                 $("#foto").hide();
                                                 $("#camara").hide();

                                              },                                              
                                                  beforeSend:function()
                                                  {                  
                                                    
                                                  }   
                                              });
                                              
                                            
                                    }
                                  }
                            },
                            beforeSend:function()
                            {                  
                                $("#rs_registro").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                            }
                          });
                   
          }else
          {
          alert("Selecciona Foto");
          }
        }
    });

	});
</script>


<style>
#camara
{
    width: 200px;
    min-height: 200px;
    border: 1px solid #008000;    
    display: none;
}

#foto
{
    width: 170px;
    height: 170px;    
    border: 1px solid #008000;
    display: none;
}

#btns_control_camara
{
    display:  none;
}

#input_file_img
{
  display:none;
}

#uploadPreview
{
    width: 170px;
    min-height: 170px;
    border: 1px solid #008000;    
    
}
</style>



<div class="row" id="controles_camara">  
  <div class="col-md-3 col-xs-0"></div>
  <div class="col-md-3 col-xs-0"></div>
  <div class="col-md-3 col-xs-10"><input id="b_personal" type="text" class="form-control" placeholder="Buscar...">  </div>
  <div class="col-md-3 col-xs-2"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_nuevo_personal"><span class="glyphicon glyphicon-plus"></span></button></div>
</div>

<div id="lista_pacientes">
<?php
include_once('../controller/personal/lista_personal.php');
?>
</div>




<style>
#camara_a
{
    width: 200px;
    min-height: 200px;
    border: 1px solid #008000;    
    display: none;
}

#foto_a
{
    width: 170px;
    height: 170px;    
    border: 1px solid #008000;
    display: none;
}

#btns_control_camara_a
{
    display:  none;
}

#input_file_img_a
{
  display:none;
}

#uploadPreview_a
{
    width: 170px;
    min-height: 170px;
    border: 1px solid #008000;    
    
}
</style>

<!-- Modal -->

<div class="modal fade" id="modal_nuevo_personal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Nuevo Personal </h4>
        </div>
        <div class="modal-body">

        <!--<form id="image_user_form_file" method="post" enctype="multipart/form-data">-->
            <div id='input_file_img' class="col-md-12">
              <input type="file" name="file_img" id="file_img">              
            </div>
        <!--</form>-->

          <form id="nuevo_personal_form" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" >
 
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto" class="contenedor col-md-12" >         
                      
                      <canvas id="foto" ></canvas>
                      <video id="camara" autoplay controls></video>
                      <img src="../recursos/img/user_nuevo.png" id="uploadPreview" />

                  </div>  

                  <div id='btns_control_camara' class="col-md-12 col-md-offset-4" >
                    <!--<input id='botonIniciar' type='button' value = 'Iniciar' class="btn btn-success btn-sm"></input>-->
                    <!--<input id='botonDetener' type='button' value = 'Detener' class="btn btn-success btn-sm"></input>-->
                    <span id='botonFoto' type='button'  class="btn btn-success btn-sm glyphicon glyphicon-record"></span>
                    <span id='botonFile' type='button'  class="btn btn-success btn-sm glyphicon glyphicon-picture"></span>
                  </div>
                                
                  <div class="row">
                    <div class="col-md-6 col-xs-6" >  
                        <!--<input type="radio" name="sex" value="s_camara">-->
                        <span id="s_camara" class="glyphicon glyphicon-camera btn btn-Warning XSmall"></span>                      
                    </div> 
                    <div class="col-md-6 col-xs-6">  
                        <!--<input type="radio" name="sex" value="s_file">-->
                        <span id="s_file" class="glyphicon glyphicon-picture btn btn-Warning  XSmall"></span>                      
                    </div> 
                  </div>
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <input name="nombre" type="text" class="form-control" id="nombre" placeholder="Nombre(s)">                  
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <input name="ap_paterno" type="text" class="form-control" id="ap_paterno" placeholder="Ap. Paterno">                  
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <input name="ap_materno" type="text" class="form-control" id="ap_materno" placeholder="Ap. Materno">                    
                      </div> 
                  </div>     

                  <div class="col-md-4 col-xs-12">    
                        <div class="form-group">                 
                        <input name="edad" type="text" class="form-control" id="edad" placeholder="Edad">                    
                      </div>
                  </div>

                   <div class="col-md-12 col-xs-12">    
                        <div class="form-group">                 
                        <input name="rfc" type="text" class="form-control" id="rfc" placeholder="RFC">                    
                      </div>
                  </div>

                  <div class="col-md-12 col-xs-12">    
                        <div class="form-group">                 
                        <input name="t_personal" type="text" class="form-control" id="t_personal" placeholder="Puesto (Doctor, Tecnico, etc)">         

                      </div>
                  </div>

              </div> 

            </div>

           <br>

            <div class="accordion" id="accordion2">

              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne_a">
                      Dirección
                    </a>
                  </div>
                  <div id="collapseOne_a" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="estado" type="text" class="form-control" id="estado" placeholder="Estado">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ciudad" type="text" class="form-control" id="ciudad" placeholder="Ciudad">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="colonia" type="text" class="form-control" id="colonia" placeholder="Colonia">                  
                          </div>
                        </div>
                    
                        <div class="col-md-8">    
                            <div class="form-group">                                   
                              <input name="calle" type="text" class="form-control" id="calle" placeholder="Calle">                    
                            </div> 
                        </div>     

                        <div class="col-md-4">   
                                <div class="form-group">                  
                                <input name="numero_calle" type="text" class="form-control" id="numero_calle" placeholder="#">                    
                                </div>
                        </div>

                        <div class="col-md-8">   
                                <div class="form-group">                  
                                <input name="cp" type="text" class="form-control" id="cp" placeholder="Codigo Postal">                    
                                </div>
                        </div>

                    </div>

                    </div>
                  </div>
              </div>


              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo_a">
                      Telefono
                    </a>
                  </div>
                  <div id="collapseTwo_a" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        
                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <input name="telefono_m" type="text" class="form-control" id="telefono_m" placeholder="Tel. Movil">                    
                            </div> 
                        </div>     

                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <input name="telefono_p" type="text" class="form-control" id="telefono_p" placeholder="Tel. Particular">                    
                            </div> 
                        </div>  

                      </div>

                    </div>
                  </div>
              </div>


            
            </div>

            <br>
                      
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <button type="submit" class="btn btn-Primary">Registrar</button>
              </div>
            </div>

          </form>


        </div>
        <div class="modal-footer" id="rs_registro">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
</div>

<div id="actualizar_datos">
  <div class="modal fade" id="modal_actualizar_paciente" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Actualizar Personal </h4>
        </div>
        <div class="modal-body">

        <!--<form id="image_user_form_file" method="post" enctype="multipart/form-data">-->
            <div id='input_file_img_a' class="col-md-12">
              <input type="file" name="file_img_a" id="file_img_a">              
            </div>
        <!--</form>-->

          <form id="actualizar_form_paciente" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" >
 
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto_a" class="contenedor col-md-12" >         
                      
                      <canvas id="foto_a" ></canvas>
                      <video id="camara_a" autoplay controls></video>
                      <img src="../recursos/img/user_nuevo.png" id="uploadPreview_a" />

                  </div>  

                  <div id='btns_control_camara_a' class="col-md-12 col-md-offset-4" >
                    <!--<input id='botonIniciar' type='button' value = 'Iniciar' class="btn btn-success btn-sm"></input>-->
                    <!--<input id='botonDetener' type='button' value = 'Detener' class="btn btn-success btn-sm"></input>-->
                    <span id='botonFoto_a' type='button'  class="btn btn-success btn-sm glyphicon glyphicon-record"></span>
                    <span id='botonFile_a' type='button'  class="btn btn-success btn-sm glyphicon glyphicon-picture"></span>
                  </div>
                                
                  <div class="row">
                    <div class="col-md-6 col-xs-6" >  
                        <!--<input type="radio" name="sex" value="s_camara">-->
                        <span id="s_camara_a" class="glyphicon glyphicon-camera btn btn-Warning XSmall"></span>                      
                    </div> 
                    <div class="col-md-6 col-xs-6">  
                        <!--<input type="radio" name="sex" value="s_file">-->
                        <span id="s_file_a" class="glyphicon glyphicon-picture btn btn-Warning  XSmall"></span>                      
                    </div> 
                  </div>
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <input name="nombre_a" type="text" class="form-control" id="nombre_a" placeholder="Nombre(s)">                  
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <input name="ap_paterno_a" type="text" class="form-control" id="ap_paterno_a" placeholder="Ap. Paterno">                  
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <input name="ap_materno_a" type="text" class="form-control" id="ap_materno_a" placeholder="Ap. Materno">                    
                      </div> 
                  </div>     

                  <div class="col-md-4 col-xs-12">   
                        <div class="form-group">                  
                        <input name="edad_a" type="text" class="form-control" id="edad_a" placeholder="Edad">                    
                      </div>
                  </div>
              </div> 

            </div>

           <br>

            <div class="accordion" id="accordion2">

              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                      Dirección
                    </a>
                  </div>
                  <div id="collapseOne" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="estado_a" type="text" class="form-control" id="estado_a" placeholder="Estado">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ciudad_a" type="text" class="form-control" id="ciudad_a" placeholder="Ciudad">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="colonia_a" type="text" class="form-control" id="colonia_a" placeholder="Colonia">                  
                          </div>
                        </div>
                    
                        <div class="col-md-8">    
                            <div class="form-group">                                   
                              <input name="calle_a" type="text" class="form-control" id="calle_a" placeholder="Calle">                    
                            </div> 
                        </div>     

                        <div class="col-md-4">        
                                <div class="form-group">             
                                <input name="numero_calle_a" type="text" class="form-control" id="numero_calle_a" placeholder="#">                    
                                </div>
                        </div>
                    </div>

                    </div>
                  </div>
              </div>


              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                      Telefono
                    </a>
                  </div>
                  <div id="collapseTwo" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        
                          <div class="col-md-12">    
                            <div class="form-group">                                   
                              <input name="telefono_m_a" type="text" class="form-control" id="telefono_m" placeholder="Tel. Movil">                    
                            </div> 
                        </div>     

                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <input name="telefono_p_a" type="text" class="form-control" id="telefono_p" placeholder="Tel. Particular">                    
                            </div> 
                        </div>    
                      </div>

                    </div>
                  </div>
              </div>              

            </div>

            <br>
                      
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <button type="submit" class="btn btn-Primary">Actualizar</button>
              </div>
            </div>

          </form>


        </div>
        <div class="modal-footer" id="rs_registro_a">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
</div>

<div id="view_datos">
</div>
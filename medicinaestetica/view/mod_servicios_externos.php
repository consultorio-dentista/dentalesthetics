<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Cvirtual</title>
    <link rel="shortcut icon" type="image/x-icon" href="../recursos/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css"/>     
    <script language="javascript" src="../recursos/js/jquery-1.9.1.js"></script>  
    <script language="javascript" src="../recursos/js/bootstrap.js"></script>  
    <script language="javascript" src="../recursos/js/jquery.validate.min.js"></script>     



     <style>
    .navbar-login
    {
        width: 305px;
        padding: 10px;
        padding-bottom: 0px;
    }
    
    .navbar-login-session
    {
        padding: 10px;
        padding-bottom: 0px;
        padding-top: 0px;
    }
    
    .icon-size
    {
        font-size: 87px;
    }
    
    #contenido
    {
        padding-top: 150px;
    }

    .navbar 
    {
        background-color: #3fb618;
    }

    
    </style>           
  </head>

    <body>
  
        <?php include("componente/menu.php"); ?>
      
        <div id="message">
        </div>

        <div id="contenido">                
            <?php include("servicios_externos/vpnc_servicios.php");?>   
        </div>    
    </body>
</html>

<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Cvirtual</title>
    <link rel="shortcut icon" type="image/x-icon" href="../recursos/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css"/>     
    <script language="javascript" src="../recursos/js/jquery-1.9.1.js"></script>  
    <script language="javascript" src="../recursos/js/bootstrap.js"></script>  
    <script language="javascript" src="../recursos/js/jquery.validate.min.js"></script>    

    <link href='../recursos/css/fullcalendar.css' rel='stylesheet' />
    <link href='../recursos/css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='../recursos/js/moment.min.js'></script>
    
    <script src='../recursos/js/fullcalendar.min.js'></script>
    <script src='../recursos/js/lang-all.js'></script>
         
     <script type="text/javascript">
     </script>
     
     
     <style>
	.navbar-login
	{
	    width: 305px;
	    padding: 10px;
	    padding-bottom: 0px;
	}
	
	.navbar-login-session
	{
	    padding: 10px;
	    padding-bottom: 0px;
	    padding-top: 0px;
	}
	
	.icon-size
	{
	    font-size: 87px;
	}
	
    #contenido
    {
        padding-top: 150px;
    }	
	</style>

    <style type="text/css">

    #loading {
        display: none;
        position: absolute;
        top: 10px;
        right: 10px;
    }

    #panel_contenido {
        max-width: 100%;   
        max-height: 70%;     
        margin: 40px auto;
        padding: 0 10px;
    }

    #script-warning {
        display: none;
        background: #eee;
        border-bottom: 1px solid #ddd;
        padding: 0 10px;
        line-height: 40px;
        text-align: center;
        font-weight: bold;
        font-size: 12px;
        color: red;
    }

   .navbar 
    {
    background-color: #3fb618;
    }



    </style>
  </head>

  <body>
  
  <?php include("componente/menu.php"); ?>
  
<div id="message">
</div>

<div id="contenido" >

    <div id='script-warning'>        
    </div>
	
    <div id='loading'>loading...</div>
    <div id='panel_contenido'>
            <div class="row">
              <div class="col-md-4">
                    <table style="width:100%" border="0">
                      <tr>
                        <td><a href="#"><img src="../recursos/img/img_sys_confing_usuarios.jpg" width="70px" class="btn_menu_sys" menu="00001"/></a></td>
                        <td></td>
                        <td></td>                        
                      </tr>
                       <tr>
                        <td></td>
                        <td></td>  
                        <td></td>                        
                      </tr>
                       <tr>
                        <td></td>
                        <td></td>  
                        <td></td>
                      </tr>
                       <tr>
                        <td></td>
                        <td></td>  
                        <td></td>
                      </tr>
                       <tr>
                        <td></td>
                        <td></td>  
                        <td></td>
                      </tr>
                    </table>
              </div>
              <div class="col-md-8" id="modulo_sys_config">

              </div>              
            </div>
        
    </div>
</div>

<!--Div con los contendios de los menus-->
<div id="modal_div">

</div>    


<script type="text/javascript">
$(document).ready(function(){
    
    $(".btn_menu_sys").click(function(){
        //cargar modulo usuario
        
         var data="op="+$(this).attr("menu");
         $.ajax({
                                        url: "../controller/sys_confing/getModulo.php",
                                        type: "post",
                                        data:  data,                                    
                                        success: function(data)
                                        {                 
                                            $("#modulo_sys_config").html("");                                                
                                            $("#modulo_sys_config").html(data);                                                                                                    
                                        },                                              
                                        beforeSend:function()
                                        {                  
                                           $("#modulo_sys_config").attr('src','../recursos/img/loading.gif');                                                                                
                                        }   
                                    });

                        return false;
    });
});
</script>

</body>
</html>

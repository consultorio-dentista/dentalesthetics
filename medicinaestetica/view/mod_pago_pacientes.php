<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';       
}

$id_paciente=$_GET['id_paciente'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Cvirtual</title>
    <link rel="shortcut icon" type="image/x-icon" href="../recursos/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css"/>     
    <script language="javascript" src="../recursos/js/jquery-1.9.1.js"></script>  
    <script language="javascript" src="../recursos/js/bootstrap.js"></script>  
    <script language="javascript" src="../recursos/js/jquery.validate.min.js"></script>    
         
       
     
     <style>
	.navbar-login
	{
	    width: 305px;
	    padding: 10px;
	    padding-bottom: 0px;
	}
	
	.navbar-login-session
	{
	    padding: 10px;
	    padding-bottom: 0px;
	    padding-top: 0px;
	}
	
	.icon-size
	{
	    font-size: 87px;
	}
	
    #contenido
    {
        padding-top: 150px;
    }

    .navbar 
    {
        background-color: #3fb618;
    }
	
	</style>

    <script type="text/javascript">
       $(document).ready(function(){

         $("#b_pagos_pendientes").click(function(){

            $.ajax({
                    url: "../controller/pacientes/pagos/buscar_paciente_pagos_pendientes.php",
                    type: "POST",
                    data:  "",
                    
                    success: function(data)
                    {                 
                        $("#datos_pacientes").html("");                                                 
                        $("#datos_pacientes").html(data);                 
                        
                    },                                              
                    beforeSend:function()
                    {                                       
                     $("#datos_pacientes").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...');
                     $("#reporte_pagos").html("");                                       
                    }   
                });

         });
       });
    </script>
  </head>

  <body>
  
  <?php include("componente/menu.php"); ?>
  

<div id="contenido" >	
        

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                 <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                 <a href="#"><span class="glyphicon glyphicon-search" data-toggle="modal" data-target="#modal_buscar_paciente"></span></a>
                 </div>

                 <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <a href="#" id="b_pagos_pendientes"><span class="glyphicon glyphicon-user" style="color:#FF0000;"></span></a>
                  </div>

                </div>                
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="datos_pacientes">
            
                <?php    
                      
                include('../controller/pacientes/pagos/cargar_datos_paciente.php');

                ?>  

            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" id="reporte_pagos">
                                
                <?php    
                      
                include('../controller/pacientes/pagos/historial_pagos.php');

                ?>  
            
            </div>

        </div>        
</div>



<!-- Modal -->
  <div class="modal fade" id="modal_buscar_paciente" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>      
          <h4 class="modal-title"> Buscar Paciente </h4>    
        </div>
        <div class="modal-body">
        <?php 
                include('pacientes/pagos/lista_pacientes.php');
        ?>    
          

                     

        </div>
        <div class="modal-footer" id="rs_registro_pago_historial">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
  
</body>
</html>
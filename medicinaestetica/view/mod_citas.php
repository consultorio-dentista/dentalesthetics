<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Cvirtual</title>
    <link rel="shortcut icon" type="image/x-icon" href="../recursos/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css"/>     
    <script language="javascript" src="../recursos/js/jquery-1.9.1.js"></script>  
    <script language="javascript" src="../recursos/js/bootstrap.js"></script>  
    <script language="javascript" src="../recursos/js/jquery.validate.min.js"></script>    

    <link href='../recursos/css/fullcalendar.css' rel='stylesheet' />
    <link href='../recursos/css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='../recursos/js/moment.min.js'></script>
    
    <script src='../recursos/js/fullcalendar.min.js'></script>
    <script src='../recursos/js/lang-all.js'></script>
         
     <script>
	    $( document ).ready(function() {
    	
      var fecha_inicio;
      var hora_inicio;

      var fecha_fin;
      var hora_fin;

    	var currentLangCode = 'es';
      var f = new Date();

      var libre=true;

      $("#actualizar_user").click(function(){
                $("#modal_usuario_actualizar").modal('show');
            });

      $("#actualizar_admin_form").validate({
        rules: {
            nombre_u: {
                minlength: 2,
                required: true                
            },
            ap_paterno_u: {
                minlength: 2,
                required: true
            },
            ap_materno_u: {
                minlength: 2,
                required: true
            },
            usuario_u: 
            {
                minlength: 5,
                required: true
            },
            password_u: 
            {
                minlength: 5,
                required: true
            },
            password_u_c: 
            {
                minlength: 5,
                required: true
            }            
        },
        messages: {
                    nombre_u: 
                    {
                        required: "Nombre Requerido.",
                        minlength: "Nombre Mayor a 2 Caracteres."
                    },
                    ap_paterno_u: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    ap_materno_u: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    }
                    ,
                    usuario_u: 
                    {
                        required: "Usuario Requerido.",
                        minlength: "Mayor a 5 Caracteres."  
                    },
                    password_u: 
                    {
                         required: "Password Requerido.",
                         minlength: "Mayor a 5 Caracteres."  
                    },
                    password_u_c: 
                    {
                          required: "Password Confirmación Requerido.",
                          minlength: "Mayor a 5 Caracteres."  
                    }
                  },
        highlight: function (element) 
        {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) 
        {
            element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) 
        {       
            var password=$("#password_u").val();
            var password_c=$("#password_u_c").val();
            
            if(password==password_c)
            {

                $.ajax({
                    url:"../controller/usuario/actualizar_usuario.php",
                    type:'post',
                    data:$("#actualizar_admin_form").serialize(),
                    success: function(data)
                    {
                        $("#rs_registro_actualizar_user").html(data);
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }
                }); 

            }else
            {
                    alert("Verifica Tu Password. No son iguales.");
            }
        }
    });


        $('#calendar').fullCalendar({            
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: moment(f).format('YYYY-MM-DD'),
            lang: currentLangCode,
            selectable: true,
            selectHelper: true,
            minTime:'07:00:00',
            maxTime:'21:00:00',
            slotDuration: '00:30:00',
            defaultView : 'agendaWeek',
            select: function(start, end,title) 
            {                
              
                fecha_inicio=start.format('YYYY-MM-DD');
                hora_inicio=start.format('HH:mm:ss');

                fecha_fin=end.format('YYYY-MM-DD');
                hora_fin=end.format('HH:mm:ss');
             

                 $("#modal_nueva_cita").modal('show');
                /*var title = prompt('Event Title:');
                var eventData;
                
                if (title) 
                {
                    eventData = 
                    {
                        title: title,
                        start: start,
                        end: end
                    };
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                }

                $('#calendar').fullCalendar('unselect');*/
            },
            eventClick: function(event) 
            {              
                
                if(libre)
                {
                // opens events in a popup window
                //window.open(event.url, 'gcalevent', 'width=700,height=600');
                  var id_paciente=event.id_paciente; 
                  var id_citas=event.id_citas; 
                  if(id_paciente!=1)
                   {

                          var data='id_paciente='+id_paciente+'&id_citas='+id_citas;

                            $.ajax({
                                        url: "../controller/agendacitas/cargar_datos_paciente.php",
                                        type: "get",
                                        data:  data,
                                        contentType: false,
                                        cache: false,
                                        processData:false,
                                        success: function(data)
                                        {                 
                                            $("#view_datos").html("");                                                
                                            $("#view_datos").html(data);                
                                            $("#modal_datos_paciente").modal('show');
                                            libre=true;
                                        },                                              
                                        beforeSend:function()
                                        {                  
                                         $("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       
                                         libre=false;
                                        }   
                                    });

                        return false;
                    }else
                    {
                        var data='id_paciente='+id_paciente+'&id_citas='+id_citas;

                            $.ajax({
                                        url: "../controller/agendacitas/cargar_datos_paciente_visita.php",
                                        type: "get",
                                        data:  data,
                                        contentType: false,
                                        cache: false,
                                        processData:false,
                                        success: function(data)
                                        {                 
                                            $("#view_datos").html("");                                                
                                            $("#view_datos").html(data);                
                                            $("#modal_datos_paciente").modal('show');
                                            libre=true;
                                        },                                              
                                        beforeSend:function()
                                        {                  
                                         $("#rs_registro_view").attr('src','../recursos/img/loading.gif');    
                                         libre=false;                                   
                                        }   
                                    });

                        return false;
                    }   
                }

            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events:  {
                        url: '../controller/agendacitas/getCitas.php',
                        error: function() 
                        {
                                $('#script-warning').show();
                        }
                    } ,
            eventColor: '#3fb618',
            eventDrop: function(event, delta) 
            {            
                actualizar_cita(event.start.format('YYYY-MM-DD'),event.start.format('HH:mm:ss'),event.end.format('YYYY-MM-DD'),event.end.format('HH:mm:ss'),event.id_citas);
            },
             eventResize: function(event, delta, revertFunc)
             {

              //alert(event.title + " end is now " + event.end.format());
              //alert(event.end.format('HH:mm:ss'));

                actualizar_cita(event.start.format('YYYY-MM-DD'),event.start.format('HH:mm:ss'),event.end.format('YYYY-MM-DD'),event.end.format('HH:mm:ss'),event.id_citas);

            }          
        });

        

          $("#b_paciente").keyup(function()
          { 

            $.ajax({
                type: 'post',
                url: '../controller/pacientes/buscar_paciente_citas.php',
                data: 'nombre='+$(this).val(),
                success: function(html)
                {             
                $("#l_pacientes").html(html);
                },
                beforeSend:function()
               {    
                $("#l_pacientes").html('');              
                $("#l_pacientes").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...');
               }
              });

          });

          function actualizar_cita(f_inicio,h_inicio,f_fin,h_fin,id_citas)
          {
            $.ajax({
                                url: "../controller/agendacitas/actualizar_cita.php",
                                type: "get",
                                data:  'fecha_inicio='+f_inicio+"&hora_inicio="+h_inicio+"&fecha_fin="+f_fin+"&hora_fin="+h_fin+"&id_citas="+id_citas,
                                contentType: false,
                                cache: false,
                                processData:false,
                                success: function(data)
                                {                 
                                    //$("#resultado_cita").html("");                                                
                                    //$("#resultado_cita").html(data); 

                                     $('#calendar').fullCalendar('refetchEvents');

                                },                                              
                                beforeSend:function()
                                {                  
                                    //$("#resultado_cita").attr('src','../recursos/img/loading.gif');                                       
                                }   
                            });
          }

        

          $("#form_nueva_cita").submit(function()
          {
            if($("input:radio[name=tipo_cita]:radio").is(':checked'))
                {

                    //pacientes
                    if($("input[name=tipo_cita]:checked").val()==0)
                    {

                         if($("input:radio[name=id_paciente]:radio").is(':checked'))
                            {

                              var id_paciente=$('input:radio[name=id_paciente]:checked').attr('id_paciente');
                              var asunto=$("#asunto_p").val();


                              $.ajax({
                                            url: "../controller/agendacitas/agregar_cita.php",
                                            type: "get",
                                            data:  'fecha_inicio='+fecha_inicio+"&hora_inicio="+hora_inicio+"&fecha_fin="+fecha_fin+"&hora_fin="+hora_fin+"&id_paciente="+id_paciente+"&asunto="+asunto,
                                            contentType: false,
                                            cache: false,
                                            processData:false,
                                            success: function(data)
                                            {                 
                                                $("#resultado_cita").html("");                                                
                                                $("#resultado_cita").html(data); 

                                                 $('#calendar').fullCalendar('refetchEvents');

                                            },                                              
                                            beforeSend:function()
                                            {                  
                                             $("#resultado_cita").attr('src','../recursos/img/loading.gif');                                       
                                            }   
                                        });

                            return false;

                            }else
                            {
                              alert("Selecciona Paciente");
                            }
                    }

                    //visitas
                    if($("input[name=tipo_cita]:checked").val()==1)
                    {
 
                   
                            var nombre_p_v=$("#nombre_p_c").val();
                            var ap_paterno_v=$("#ap_p_c").val();
                            var ap_materno_v=$("#am_p_c").val();
                            var asunto=$("#asunto_v").val();

                            if(nombre_p_v!="")
                            {

                              $.ajax({
                                            url: "../controller/agendacitas/agregar_cita_visita.php",
                                            type: "get",
                                            data:  "fecha_inicio="+fecha_inicio+"&hora_inicio="+hora_inicio+"&fecha_fin="+fecha_fin+"&hora_fin="+hora_fin+
                                            "&nombre_p_v="+nombre_p_v+"&ap_paterno_v="+ap_paterno_v+"&ap_materno_v="+ap_materno_v+"&asunto="+asunto,
                                            contentType: false,
                                            cache: false,
                                            processData:false,
                                            success: function(data)
                                            {                 
                                                $("#resultado_cita").html("");                                                
                                                $("#resultado_cita").html(data); 

                                                 $('#calendar').fullCalendar('refetchEvents');

                                            },                                              
                                            beforeSend:function()
                                            {                  
                                             $("#resultado_cita").attr('src','../recursos/img/loading.gif');                                       
                                            }   
                                        });

                            return false;
                            }else
                            {
                                alert("Ingresa nombre.");
                            }

                    }

                   
                }else
                {
                     alert("Selecciona Tipo Paciente");
                }



                return false;
          });

          $("#cita_visita").hide();
          $("#buscar_paciente").hide();

          $(".tipo_cita").click(function(){
                if($(this).val()==1)
                {
                    $("#cita_visita").show();
                    $("#buscar_paciente").hide();
                }else
                {
                    $("#buscar_paciente").show();
                    $("#cita_visita").hide();
                }
          });


		});
     </script>    
     
     <style>
	.navbar-login
	{
	    width: 305px;
	    padding: 10px;
	    padding-bottom: 0px;
	}
	
	.navbar-login-session
	{
	    padding: 10px;
	    padding-bottom: 0px;
	    padding-top: 0px;
	}
	
	.icon-size
	{
	    font-size: 87px;
	}
	
    #contenido
    {
        padding-top: 150px;
    }	
	</style>

    <style type="text/css">

    #loading {
        display: none;
        position: absolute;
        top: 10px;
        right: 10px;
    }

    #calendar {
        max-width: 100%;   
        max-height: 70%;     
        margin: 40px auto;
        padding: 0 10px;
    }

    #script-warning {
        display: none;
        background: #eee;
        border-bottom: 1px solid #ddd;
        padding: 0 10px;
        line-height: 40px;
        text-align: center;
        font-weight: bold;
        font-size: 12px;
        color: red;
    }

   .navbar 
    {
    background-color: #3fb618;
    }



    </style>
  </head>

  <body>
  
  <?php include("componente/menu.php"); ?>

  
<div id="message">
</div>

<div id="contenido" >

    <div id='script-warning'>      
    </div>
	
    <div id='loading'>loading...</div>
    <div id='calendar'></div>
</div>

<div id="view_datos">
<div class="modal fade" id="modal_datos_paciente" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Paciente </h4>
        </div>
        <div class="modal-body">
      
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto" class="contenedor col-md-12" >                              
                      <img src="../recursos/img/user_nuevo.png" id="uploadPreview" />
                  </div>                                
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <span class="help-block">Nombre(s):</span>
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <span class="help-block">Ap Paterno:</span>
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <span class="help-block">Ap materno:</span>
                      </div> 
                  </div>     

                  <div class="col-md-4 col-xs-12">                    
                        <span class="help-block">Edad:</span>
                  </div>
              </div> 

            </div>

           <br>

            <div class="accordion" id="accordion2">

              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne_view">
                      Dirección
                    </a>
                  </div>
                  <div id="collapseOne_view" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <span class="help-block">Estado:</span>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <span class="help-block">Ciudad:</span>
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <span class="help-block">Colonia:</span>
                          </div>
                        </div>
                    
                        <div class="col-md-8">    
                            <div class="form-group">                                   
                              <span class="help-block">Calle:</span>
                            </div> 
                        </div>     

                        <div class="col-md-4">                    
                                <span class="help-block">#:</span>
                        </div>
                    </div>

                    </div>
                  </div>
              </div>


              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo_view">
                      Telefono
                    </a>
                  </div>
                  <div id="collapseTwo_view" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        
                         <div class="col-md-12">    
                            <div class="form-group">                                   
                              <span class="help-block">Telefono:</span>
                            </div> 
                        </div>     
                      </div>

                    </div>
                  </div>
              </div>


               <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree_view">
                      Tutor
                    </a>
                  </div>
                  <div id="collapseThree_view" class="accordion-body collapse">
                    <div class="accordion-inner">
                        
                        <div class="row">
                            <div class="col-md-12"> 
                              <div class="form-group">                                
                                  <span class="help-block">Nombre(s) Tutor:</span>
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">                                
                                  <span class="help-block">Ap paterno Tutor:</span>
                              </div>
                            </div>
                        
                            <div class="col-md-12">    
                                <div class="form-group">                                   
                                  <span class="help-block">Ap materno Tutor:</span>
                                </div> 
                            </div>     

                          </div>
                      </div>

                  </div>
                </div>

            </div>

            <br>
                      
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <!--<button type="submit" class="btn btn-Primary">Registrar</button>-->
              </div>
            </div>
      

        </div>
        <div class="modal-footer" id="rs_registro_view">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
</div>








<div id="nuevo_cita">
<div class="modal fade" id="modal_nueva_cita" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Nueva Cita </h4>
        </div>
        <div class="modal-body">
      
          <form id="form_nueva_cita">
            <div class="row">
                <div class="col-md-12">
                    <input type="radio" name="tipo_cita" value="0" class="tipo_cita">Pacientes
                    <input type="radio" name="tipo_cita" value="1" class="tipo_cita">Visita
                </div>   
                <div class="col-md-12" id="buscar_paciente" >
                    <div class="form-group">
                        <input id="b_paciente" type="text" class="form-control" placeholder="Buscar Paciente...">
                    </div> 

                    <div id="l_pacientes">                                
                    </div>      

                    
                    <div class="form-group">
                        <input id="asunto_p" type="text" class="form-control" placeholder="Asunto">
                    </div>
                            
                </div>

                <div class="col-md-12" id="cita_visita" >
                    <div class="form-group">
                        <input id="nombre_p_c" type="text" class="form-control" placeholder="Nombre(s)">
                    </div>
                    <div class="form-group">
                        <input id="ap_p_c" type="text" class="form-control" placeholder="Ap Paterno">
                    </div>
                    <div class="form-group">
                        <input id="am_p_c" type="text" class="form-control" placeholder="Ap Materno">
                    </div>  
                   
                    <div class="form-group">
                            <input id="asunto_v" type="text" class="form-control" placeholder="Asunto">
                    </div>
                    
                                  
                </div>

                 

            </div>  

            <button type="submit" class="btn btn-success" id="agregar_cita">Agendar Cita</button>

          </form>

        </div>
        <div class="modal-footer" id="rs_registro_view">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
          
          <div id="resultado_cita">
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>




<!-- Modal -->
  <div class="modal fade" id="modal_usuario_actualizar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Admin </h4>
        </div>
        <div class="modal-body">

          <form id="actualizar_admin_form" class="form-horizontal" role="form" method="post"> 
          <input name="id_persona" type="hidden" value="<?php echo $_SESSION['id_persona'];?>">                        
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="nombre_u" type="text" class="form-control" id="nombre_u" placeholder="Nombre(s):" value="<?php echo $_SESSION['nombre_u'];?>">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_paterno_u" type="text" class="form-control" id="ap_paterno_u" placeholder="Ap Paterno:" value="<?php echo $_SESSION['ap_paterno_u'];?>">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_materno_u" type="text" class="form-control" id="ap_materno_u" placeholder="Ap Materno:" value="<?php echo $_SESSION['ap_materno_u'];?>">                  
                          </div>
                        </div>
                    
                        <div class="col-md-7">    
                            <div class="form-group">                                   
                              <input name="usuario_u" type="text" class="form-control" id="usuario_u" placeholder="Usuario:" value="<?php echo $_SESSION['usuario'];?>">                    
                            </div> 
                        </div>     

                        

                        <div class="col-md-7">  
                                <div class="form-group">                   
                                    <input name="password_u" type="password" class="form-control" id="password_u" placeholder="Password">                    
                                </div>
                        </div>
                       

                        <div class="col-md-7">                    
                                <div class="form-group"> 
                                    <input name="password_u_c" type="password" class="form-control" id="password_u_c" placeholder="Password Confirmar">                    
                                </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-offset-9 col-md-12">
                            <button type="submit" class="btn btn-Primary">Actualizar</button>
                            </div>
                        </div>  
                    </div>
          </form>

                     


        </div>
        <div class="modal-footer" id="rs_registro_actualizar_user">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>

</body>
</html>

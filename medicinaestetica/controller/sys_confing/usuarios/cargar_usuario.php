<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}



include_once('../../db/mysql.php');
$db=new MySQL();

$id_usuario=$_POST['id_usuario'];


$sql = "select * from usuario U, persona P where U.id_usuario=".$id_usuario." and U.persona_id_persona=P.id_persona";
$consulta=$db->consulta($sql);

$usuario;
$nombre;
$ap_paterno;
$ap_materno;

if($db->num_rows($consulta)>0)
	{
	  	while($resultados = $db->fetch_array($consulta))
	  	{
	  		$usuario=$resultados["usuario"];
	  		$nombre=$resultados["nombre"];
	  		$ap_paterno=$resultados["ap_paterno"];
	  		$ap_materno=$resultados["ap_materno"];
	  	}
	}

?>


<div class="modal fade" id="modal_usuario_actualizar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Usuario </h4>
        </div>
        <div class="modal-body">

          <form id="actualizar_admin_form" class="form-horizontal" role="form" method="post"> 
          <input name="id_usuario" type="hidden" value="<?php echo $id_usuario;?>">                        
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="nombre_u" type="text" class="form-control" id="nombre_u" placeholder="Nombre(s):" value="<?php echo $nombre; ?>">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_paterno_u" type="text" class="form-control" id="ap_paterno_u" placeholder="Ap Paterno:" value="<?php echo $ap_paterno;?>">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_materno_u" type="text" class="form-control" id="ap_materno_u" placeholder="Ap Materno:" value="<?php echo $ap_materno;?>">                  
                          </div>
                        </div>
                    
                        <div class="col-md-7">    
                            <div class="form-group">                                   
                              <input name="usuario_u" type="text" class="form-control" id="usuario_u" placeholder="Usuario:" value="<?php echo $usuario;?>">                    
                            </div> 
                        </div>     

                        

                        <div class="col-md-7">  
                                <div class="form-group">                   
                                    <input name="password_u" type="password" class="form-control" id="password_u" placeholder="Password">                    
                                </div>
                        </div>
                       

                        <div class="col-md-7">                    
                                <div class="form-group"> 
                                    <input name="password_u_c" type="password" class="form-control" id="password_u_c" placeholder="Password Confirmar">                    
                                </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-offset-9 col-md-12">
                            <button type="submit" class="btn btn-Primary">Actualizar</button>
                            </div>
                        </div>  
                    </div>
          </form>

                     


        </div>
        <div class="modal-footer" id="rs_registro_actualizar_user">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>

  <script type="text/javascript">
  $(document).ready(function(){

    $("#actualizar_admin_form").validate({
        rules: {
            nombre_u: {
                minlength: 2,
                required: true                
            },
            ap_paterno_u: {
                minlength: 2,
                required: true
            },
            ap_materno_u: {
                minlength: 2,
                required: true
            },
            usuario_u: 
            {
                minlength: 5,
                required: true
            },
            password_u: 
            {
                minlength: 5,
                required: true
            },
            password_u_c: 
            {
                minlength: 5,
                required: true
            }            
        },
        messages: {
                    nombre_u: 
                    {
                        required: "Nombre Requerido.",
                        minlength: "Nombre Mayor a 2 Caracteres."
                    },
                    ap_paterno_u: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    ap_materno_u: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    }
                    ,
                    usuario_u: 
                    {
                        required: "Usuario Requerido.",
                        minlength: "Mayor a 5 Caracteres."  
                    },
                    password_u: 
                    {
                         required: "Password Requerido.",
                         minlength: "Mayor a 5 Caracteres."  
                    },
                    password_u_c: 
                    {
                          required: "Password Confirmación Requerido.",
                          minlength: "Mayor a 5 Caracteres."  
                    }
                  },
        highlight: function (element) 
        {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) 
        {
            element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) 
        {       
            var password=$("#password_u").val();
            var password_c=$("#password_u_c").val();
            
            if(password==password_c)
            {

                $.ajax({
                    url:"../controller/sys_confing/usuarios/actualizar_usuario.php",
                    type:'post',
                    data:$("#actualizar_admin_form").serialize(),
                    success: function(data)
                    {
                        $("#rs_registro_actualizar_user").html(data);                         
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }
                }); 

            }else
            {
                    alert("Verifica Tu Password. No son iguales.");
            }
        }
    });

    $('#modal_usuario_actualizar').on('hidden.bs.modal', function () {
    // do something…
       cargar_lista_usuarios();
    });

     function cargar_lista_usuarios()
  {
                    var data="op=00001";
                   $.ajax({
                                                  url: "../controller/sys_confing/getModulo.php",
                                                  type: "post",
                                                  data:  data,                                    
                                                  success: function(data)
                                                  {                 
                                                      $("#modulo_sys_config").html("");                                                
                                                      $("#modulo_sys_config").html(data);                                                                                                    
                                                  },                                              
                                                  beforeSend:function()
                                                  {                  
                                                     $("#modulo_sys_config").attr('src','../recursos/img/loading.gif');                                                                                
                                                  }   
                                              });

                                  return false;
  }
  });
  </script>
<?php


session_start();

include_once('../../../controller/db/mysql.php');
$db=new MySQL();

//

$sql = "SELECT * FROM historial_pagos_rehabilitacion 
 WHERE paciente_id_paciente = ".$id_paciente." 
 order by id_historial_pagos_rehabilitacion ASC";

echo '<div id="h_pagos">';
echo '<button type="button" class="btn_pagos btn btn-success" id="historial_pagos_rehabilitacion" id_paciente="'.$id_paciente.'"><span class="glyphicon glyphicon-plus"></span></button>';
echo '<br><br>';
echo '<div id="accordion" role="tablist" aria-multiselectable="true">';


$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  $num_rows=$db->num_rows($consulta);
  $count=1;
  	while($resultados = $db->fetch_array($consulta))
  	{ 
      
      if($count!=$num_rows)      
      {
   			echo '<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$resultados["id_historial_pagos_rehabilitacion"].'" aria-expanded="true" aria-controls="collapseOne">
                              Tratamiento Fecha Inicio : '.$resultados["fecha_inicio"].'
                            </a>

                             
                               
                          </h4>


                        </div>
                        <div id="collapse'.$resultados["id_historial_pagos_rehabilitacion"].'" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                          


             <table class="table" >
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Tratamiento</th>
                  <th>Cantidad</th>
                  <th>Costo U.</th>
                  <th>SubTotal</th>
                  <th>Abono</th>
                  <th></th>
                                  
                </tr>
              </thead>
              <tbody>';


               $sql = "SELECT * FROM registro_pagos_rehabilitacion 
              WHERE historial_pagos_rehabilitacion_id_historial_pagos_rehabilitacion = ".$resultados ['id_historial_pagos_rehabilitacion']." 
              AND estatus='a';";
           
              $costo_total=0;  
              $total_abono=0;

              $consulta_r = $db->consulta($sql);
              if($db->num_rows($consulta_r)>0)
              {
                  while($resultados_r = $db->fetch_array($consulta_r))
                  { 
                      echo '<tr>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0001" valor="'.$resultados_r["fecha"].'">'.$resultados_r["fecha"].'</td>';

                          if($resultados_r["tratamiento"]=="*")
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0010" valor=""></td>';
                          }else
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0010" valor="'.$resultados_r["tratamiento"].'">'.$resultados_r["tratamiento"].'</td>'; 
                          }

                      echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0011" valor="'.$resultados_r["cantidad"].'">'.$resultados_r["cantidad"].'</td>                      
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0100" valor="'.$resultados_r["costo"].'">$ '.$resultados_r["costo"].'</td>
                          <td>$'.$resultados_r["cantidad"]*$resultados_r["costo"].'</td>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0101" valor="'.$resultados_r["abono"].'">$ '.$resultados_r["abono"].'</td>
                          <td class="borrar_rh" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'"></td>
                          
                        </tr>';
                        $costo_total+=$resultados_r["cantidad"]*$resultados_r["costo"];
                        $total_abono+=$resultados_r["abono"];

                  }
              }


               echo '<tr>
                          <td></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Total:</b></td>
                          <td>$ '.$costo_total.'</td>
                          <td></td>
                          <td></td>

                          
                          
                      </tr>
                      <tr>
                          <td></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Abono:</b></td>
                          <td>$ '.$total_abono.'</td>
                          <td></td>
                          <td></td>

                         
                          
                      </tr>
                      <tr>
                          <td><button type="button" class="btn_nuevo_registro btn btn-warning"  id_historial_pagos_rehabilitacion="'.$resultados["id_historial_pagos_rehabilitacion"].'"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></button></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Adeudo:</b></td>';

                           if($costo_total>=$total_abono)
                          {
                            echo '<td>$'.($costo_total-$total_abono).'</td>';
                          }else
                          {
                             echo '<td>$+'.(-1*($costo_total-$total_abono)).'</td>';
                          }
                          
                          
                          
                      echo '<td></td><td></td></tr>';

              

      echo ' </tbody>
            </table>


                        </div>
                      </div>';  

                      $count++;
      }else
      {

          echo '<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$resultados["id_historial_pagos_rehabilitacion"].'" aria-expanded="true" aria-controls="collapseOne">
                              Tratamiento Fecha Inicio : '.$resultados["fecha_inicio"].'
                            </a>

                             
                               
                          </h4>


                        </div>
                        <div id="collapse'.$resultados["id_historial_pagos_rehabilitacion"].'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          


             <table class="table">
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Tratamiento</th>
                  <th>Cantidad</th>
                  <th>Costo U.</th>
                  <th>SubTotal</th>
                  <th>Abono</th>
                  <th></th>
                                  
                </tr>
              </thead>
              <tbody>';


                $sql = "SELECT * FROM registro_pagos_rehabilitacion 
              WHERE historial_pagos_rehabilitacion_id_historial_pagos_rehabilitacion = ".$resultados ['id_historial_pagos_rehabilitacion']." 
              AND estatus='a';";
           
              $costo_total=0;  
              $total_abono=0;

              $consulta_r = $db->consulta($sql);
              if($db->num_rows($consulta_r)>0)
              {
                  while($resultados_r = $db->fetch_array($consulta_r))
                  { 
                      echo '<tr>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0001" valor="'.$resultados_r["fecha"].'">'.$resultados_r["fecha"].'</td>';

                          if($resultados_r["tratamiento"]=="*")
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0010" valor=""></td>';
                          }else
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0010" valor="'.$resultados_r["tratamiento"].'">'.$resultados_r["tratamiento"].'</td>'; 
                          }

                      echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0011" valor="'.$resultados_r["cantidad"].'">'.$resultados_r["cantidad"].'</td>                      
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0100" valor="'.$resultados_r["costo"].'">$ '.$resultados_r["costo"].'</td>
                          <td>$'.$resultados_r["cantidad"]*$resultados_r["costo"].'</td>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'" tipo="0101" valor="'.$resultados_r["abono"].'">$ '.$resultados_r["abono"].'</td>
                          <td class="borrar_rh" id_registro="'.$resultados_r["id_registro_pagos_rehabilitacion"].'"></td>
                          
                        </tr>';
                        $costo_total+=$resultados_r["cantidad"]*$resultados_r["costo"];
                        $total_abono+=$resultados_r["abono"];

                  }
              }


                echo '<tr>
                          <td></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Total:</b></td>
                          <td>$ '.$costo_total.'</td>
                          <td></td>
                           <td></td>


                          
                          
                      </tr>
                      <tr>
                          <td></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Abono:</b></td>
                          <td>$ '.$total_abono.'</td>
                          <td></td>
                          <td></td>
                         
                          
                      </tr>
                      <tr>
                          <td><button type="button" class="btn_nuevo_registro btn btn-warning"  id_historial_pagos_rehabilitacion="'.$resultados["id_historial_pagos_rehabilitacion"].'"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></button></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Adeudo:</b></td>';

                           if($costo_total>=$total_abono)
                          {
                            echo '<td>$'.($costo_total-$total_abono).'</td>';
                          }else
                          {
                             echo '<td>$+'.(-1*($costo_total-$total_abono)).'</td>';
                          }                                                                                                
                      echo'<td></td><td></td></tr>';

              

      echo ' </tbody>
            </table>


                        </div>
                      </div>';  
      }
 	}
}


echo '</div>';
echo '</div>';


?>

<script type="text/javascript">
$(document).ready(function()
{
var id_paciente=0;

$("#historial_pagos_rehabilitacion").click(function()
{    
    id_paciente=$("#historial_pagos_rehabilitacion").attr('id_paciente');    

     $.ajax({
                    url:"../controller/pacientes/pagos/nuevo_historial_pagos.php",
                    type:"post",
                    data: "id_paciente="+id_paciente,
                    success: function(data)
                    {
                          
                            $.ajax({
                                  url: "../controller/pacientes/pagos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_paciente="+id_paciente,
                                  
                                  success: function(data)
                                  {                 
                                    $("#reporte_pagos").html("");                                               
                                    $("#reporte_pagos").html(data);               
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                   $("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                            
                            alert(data);

                          return false;      
                        //volver a cargar historiales
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }                    
                }); 

  });


$(".btn_nuevo_registro").click(function()
{    
    id_historial_pagos_rehabilitacion=$(this).attr('id_historial_pagos_rehabilitacion');    
    id_paciente=$("#historial_pagos_rehabilitacion").attr('id_paciente');   
    
    $.ajax({
                    url:"../controller/pacientes/pagos/nuevo_historial_registro_pago.php",
                    type:"post",
                    data: "id_historial_pagos_rehabilitacion="+id_historial_pagos_rehabilitacion,
                    success: function(data)
                    {
                          
                            $.ajax({
                                  url: "../controller/pacientes/pagos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_paciente="+id_paciente,
                                  
                                  success: function(data)
                                  {                 
                                    $("#reporte_pagos").html("");                                               
                                    $("#reporte_pagos").html(data);               
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                   $("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                            
                            

                          return false;      
                        //volver a cargar historiales
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }                    
                }); 

  });


  $(".edit_celda").click(function () 
  {                 
                    var id_registro=$(this).attr('id_registro');
                    var tipo=$(this).attr('tipo');
                    var OriginalContent = $(this).attr('valor');
                    
                    $(this).addClass("cellEditing");
                    $(this).html("<input style='width:100px;' type='text' value='"+OriginalContent +"'/>");
                                       
                                       
                    $(this).children().first().keypress(function (e) 
                    {                      
                        if (e.which == 13) 
                        {
                            var newContent = $(this).val();
                            
                            $(this).parent().attr('cantidad',newContent);                            
                            $(this).parent().text(newContent);
                            $(this).parent().removeClass("cellEditing");
                            
                            //guardra cambios 
                            $.ajax({
                            type: 'post',
                            url:"../controller/pacientes/pagos/edit_celda_historial_pagos.php",
                            data:  'tipo='+tipo+'&id_registro='+id_registro+"&valor="+newContent,
                            success: function(content)
                                {
                                    //alert(content);
                                  id_paciente=$("#historial_pagos_rehabilitacion").attr('id_paciente');    
                                     $.ajax({
                                  url: "../controller/pacientes/pagos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_paciente="+id_paciente,
                                  
                                  success: function(data)
                                  {                 
                                    $("#reporte_pagos").html("");                                               
                                    $("#reporte_pagos").html(data);               
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                   $("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                                }
                            });
                            return false;  
                        }
                       
                    });

                 
                                        
                    $(this).children().first().blur(function(){
                        $(this).parent().text(OriginalContent);
                        $(this).parent().removeClass("cellEditing");  
                       
                    });
                    
                    $(this).children().first().focus();

                                        
  });


 $('.borrar_rh').dblclick(function(){

    var r = confirm("¿Está seguro de eliminar?");
    if (r == true) 
    {
    var id_registro=$(this).attr("id_registro");
    

          $.ajax({
                    url:"../controller/pacientes/pagos/eliminar_registro_historial_pagos.php",
                    type:"post",
                    data: "id_registro="+id_registro,
                    success: function(data)
                    {
                          id_paciente=$("#historial_pagos_rehabilitacion").attr('id_paciente');   

                            $.ajax({
                                  url: "../controller/pacientes/pagos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_paciente="+id_paciente,
                                  
                                  success: function(data)
                                  {                 
                                    $("#reporte_pagos").html("");                                               
                                    $("#reporte_pagos").html(data);               
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                   $("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                            
                            //alert(data);

                          return false;      
                        //volver a cargar historiales
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }                    
                }); 
    }

 });


});

</script>

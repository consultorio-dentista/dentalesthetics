<?php
	
	$id_paciente=$_POST['id_paciente'];

	$nombre=$_POST['nombre_a'];
	$ap_paterno=$_POST['ap_paterno_a'];
	$ap_materno=$_POST['ap_materno_a'];
	$edad=$_POST['edad_a'];

	$estado=$_POST['estado_a'];
	$ciudad=$_POST['ciudad_a'];
	$colonia=$_POST['colonia_a'];
	$calle=$_POST['calle_a'];
	$numero_calle=$_POST['numero_calle_a'];
	
	$telefono_m=$_POST['telefono_m_a'];
	$telefono_p=$_POST['telefono_p_a'];

	$nombre_tutor=$_POST['nombre_tutor_a'];
	$ap_paterno_tutor=$_POST['ap_paterno_tutor_a'];
	$ap_materno_tutor=$_POST['ap_materno_tutor_a'];

	$telefono_m_t=$_POST['telefono_m_t_a'];
	$telefono_p_t=$_POST['telefono_p_t_a'];

	$rfc=$_POST['rfc_a'];
    $cp=$_POST['cp_a'];

    //insertar direccion
    if($cp=="")
    {
        $cp="sin registro";
    }

    if($rfc=="")
    {
        $rfc="sin registro";
    }


	include_once('../db/mysql.php');
	$db=new MySQL();

	//Buscar datos de paciente  buscar el id_persona
	
	$id_persona;
	$sql="SELECT * FROM paciente WHERE id_paciente = ".$id_paciente;

	$consulta = $db->consulta($sql);
	if($db->num_rows($consulta)>0)
	{
	  	while($resultados = $db->fetch_array($consulta))
	  	{ 
	       $id_persona=$resultados['persona_id_persona'];	       
	 	}
	}

	$id_direccion;
	$id_foto;

	$sql="SELECT * FROM persona WHERE id_persona =".$id_persona;
	$consulta = $db->consulta($sql);
	if($db->num_rows($consulta)>0)
	{
	  	while($resultados = $db->fetch_array($consulta))
	  	{ 
	       $id_direccion=$resultados['direccion_id_direccion'];
	       $id_foto= $resultados['foto_id_foto'];	       
	 	}
	}

	//Actualizar Datos Persona
	$sql="UPDATE persona SET nombre = '".$nombre."', 
	ap_paterno = '".$ap_paterno."', 
	ap_materno = '".$ap_materno."', 
	edad = '".$edad."',
	rfc = '".$rfc."' 
	WHERE id_persona =".$id_persona;
	$db->consulta($sql);


	//Actualiar direccion
	$sql="UPDATE direccion SET estado = '".$estado."',
	ciudad = '".$ciudad."', 
	colonia = '".$colonia."', 
	calle = '".$calle."', 
	numero = '".$numero_calle."', 
	cp = '".$cp."' 
	WHERE id_direccion = ".$id_direccion;
	$db->consulta($sql);

	//actualizar telefono

	$id_telefono_m;
	$sql="SELECT * 
	FROM paciente_has_telefono PHT, telefono T, tipo_telefono TT
	WHERE PHT.paciente_id_paciente = ".$id_paciente."
	AND PHT.telefono_id_telefono=T.id_telefono 
	AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
	AND TT.id_tipo_telefono=1";

	$consulta = $db->consulta($sql);
	if($db->num_rows($consulta)>0)
	{
	  	while($resultados = $db->fetch_array($consulta))
	  	{ 
	             $id_telefono_m=$resultados['telefono_id_telefono'];
	 	}
	}

	$sql="UPDATE telefono 
	SET telefono = '".$telefono_m."' 
	WHERE id_telefono =".$id_telefono_m;
	$db->consulta($sql);

	$id_telefono_p;
	$sql="SELECT * 
	FROM paciente_has_telefono PHT, telefono T, tipo_telefono TT
	WHERE PHT.paciente_id_paciente = ".$id_paciente." 
	AND PHT.telefono_id_telefono=T.id_telefono
	AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
	AND TT.id_tipo_telefono=2";

	$consulta = $db->consulta($sql);
	if($db->num_rows($consulta)>0)
	{
	  	while($resultados = $db->fetch_array($consulta))
	  	{ 
	             $id_telefono_p=$resultados['telefono_id_telefono'];
	 	}
	}

	$sql="UPDATE telefono 
	SET telefono = '".$telefono_p."' 
	WHERE id_telefono =".$id_telefono_p;
	$db->consulta($sql);


	//actualizar datos tutor
	//verificar si hay tutor
	if($nombre_tutor!="")
    {
    	//buscar el id_persona
	    	$id_persona_tutor=0;
	    	$id_tutor=0;
	    	$sql="SELECT * FROM tutor WHERE paciente_id_paciente =".$id_paciente;
	    	//echo $sql;
	    	$consulta = $db->consulta($sql);
			if($db->num_rows($consulta)>0)
			{
			  	while($resultados = $db->fetch_array($consulta))
			  	{ 
			             $id_persona_tutor=$resultados['persona_id_persona'];
			             $id_tutor= $resultados['id_tutor'];
			 	}
			}

			

			if($id_persona_tutor==0)//no existe agregar como nuevo
			{
					$sql="INSERT INTO persona (id_persona, direccion_id_direccion, foto_id_foto, nombre, ap_paterno, ap_materno, edad,rfc) 
				    VALUES (NULL, '".$id_direccion."', '1', '".$nombre_tutor."', '".$ap_paterno_tutor."', '".$ap_materno_tutor."', 'SR','SR');";
				    $db->consulta($sql);
				    $id_persona_tutor=$db->insert_id();


				    $sql="INSERT INTO tutor (id_tutor, persona_id_persona, paciente_id_paciente) 
				    VALUES (NULL , '".$id_persona_tutor."', '".$id_paciente."');";
				    $db->consulta($sql);	
				    $id_tutor=$db->insert_id();


			}else
			{

	

				//actualizar datos
				$sql="UPDATE persona SET nombre = '".$nombre_tutor."', 
				ap_paterno = '".$ap_paterno_tutor."',
				ap_materno = '".$ap_materno_tutor."', 
				edad = 'SR.' 
				WHERE id_persona =".$id_persona_tutor;
				$db->consulta($sql);

			}



			//buscar telefono Movil

			$id_telefono_m_t=0;
			$sql="SELECT * 
			FROM tutor_has_telefono THT, telefono T, tipo_telefono TT
			WHERE THT.tutor_id_tutor = ".$id_tutor." 
			AND THT.telefono_id_telefono=T.id_telefono
			AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
			AND TT.id_tipo_telefono=1";

			$consulta = $db->consulta($sql);
			if($db->num_rows($consulta)>0)
			{
			  	while($resultados = $db->fetch_array($consulta))
			  	{ 
			             $id_telefono_m_t=$resultados['telefono_id_telefono'];
			 	}
			}

			if($id_telefono_m_t==0) //insertar como nuevo telefono
			{
					
					$sql="INSERT INTO telefono (id_telefono, tipo_telefono_id_tipo_telefono, telefono) 
		            VALUES (NULL, '1', '".$telefono_m_t."');";
		            $db->consulta($sql);
		            $id_telefono_m_t=$db->insert_id();

		            $sql="INSERT INTO tutor_has_telefono (tutor_id_tutor, telefono_id_telefono) VALUES ('".$id_tutor."', '".$id_telefono_m_t."')";
		            $db->consulta($sql);

			}else //actualizar telefono 
			{
				$sql="UPDATE telefono 
				SET telefono = '".$telefono_m_t."' 
				WHERE id_telefono =".$id_telefono_m_t;
				$db->consulta($sql);
			}


			$id_telefono_p_t=0;
			$sql="SELECT * 
			FROM tutor_has_telefono THT, telefono T, tipo_telefono TT
			WHERE THT.tutor_id_tutor = ".$id_tutor." 
			AND THT.telefono_id_telefono=T.id_telefono
			AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
			AND TT.id_tipo_telefono=2";

			$consulta = $db->consulta($sql);
			if($db->num_rows($consulta)>0)
			{
			  	while($resultados = $db->fetch_array($consulta))
			  	{ 
			             $id_telefono_p_t=$resultados['telefono_id_telefono'];
			 	}
			}

			if($id_telefono_p_t==0) //insertar como nuevo telefono
			{
					
					$sql="INSERT INTO telefono (id_telefono, tipo_telefono_id_tipo_telefono, telefono) 
		            VALUES (NULL, '2', '".$telefono_p_t."');";
		            $db->consulta($sql);
		            $id_telefono_p_t=$db->insert_id();

		            $sql="INSERT INTO tutor_has_telefono (tutor_id_tutor, telefono_id_telefono) VALUES ('".$id_tutor."', '".$id_telefono_p_t."')";
		            $db->consulta($sql);

			}else //actualizar telefono 
			{
				$sql="UPDATE telefono 
				SET telefono = '".$telefono_p_t."' 
				WHERE id_telefono =".$id_telefono_p_t;
				$db->consulta($sql);
			}

    }

	echo $id_persona;

?>
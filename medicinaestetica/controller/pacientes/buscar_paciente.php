<?php
session_start();
$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000000101";
include("../permisos/validar_permiso.php");

if($permiso==1)
{
	

$nombre=$_POST['nombre'];

include_once('../db/mysql.php');
$db=new MySQL();

$sql="select PA.*, PE.*, FT.* 
from paciente PA, persona PE, foto FT 
where PA.persona_id_persona=PE.id_persona 
and PE.foto_id_foto=FT.id_foto 
and PA.estatus='a'
and (PE.nombre LIKE '%".$nombre."%' 
or PE.ap_paterno LIKE '%".$nombre."%' 
or PE.ap_materno LIKE '%".$nombre."%') limit 150";


echo '<div class="table-responsive"> 
	<table class="table">
	    <thead>
	        <tr>
	            <th>Foto</th>
	            <th>Nombre</th>
	            <th>Apellido Paterno</th>
	            <th>Apellido Materno</th>
	            <th></th>
	        </tr>
	    </thead>

	    <tbody>';


$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  	while($resultados = $db->fetch_array($consulta))
  	{ 
   			echo '<tr>
	            <td align="center"><img class="view_info img-circle" src="../view/pacientes/foto/'.$resultados["url"].'?nocache=1" width="100px" height="100px" id_paciente="'.$resultados["id_paciente"].'" ></td>
	            <td>'.$resultados["nombre"].'</td>
	            <td>'.$resultados["ap_paterno"].'</td>
	            <td>'.$resultados["ap_materno"].'</td>
	            <td>
	            <button type="button" class="btn_pagos_paciente btn btn-warning" id_paciente="'.$resultados["id_paciente"].'"><span class="glyphicon glyphicon-usd"></span></button>
	            <button type="button" class="btn_actualizar btn btn-primary" id_paciente="'.$resultados["id_paciente"].'"><span class="glyphicon glyphicon-cog"></span></button>
	            <button type="button" class="btn_eliminar btn btn-danger" id_paciente="'.$resultados["id_paciente"].'"><span class="glyphicon glyphicon-minus"></span></button></td>
	        </tr>';	
 	}
}


echo '    </tbody>
	</table>
</div>';

}else
{
	echo '<script type="text/javascript">
			alert("Error: Sin Permiso Activado ");
			</script>';		
}


?>

<script type="text/javascript">
	$(".btn_actualizar").click(function(){		
		
		var id_paciente_actualizar=$(this).attr('id_paciente');		
		var data='id_paciente='+id_paciente_actualizar;
		
		$.ajax({
		            url: "../controller/pacientes/cargar_datos_paciente_actualizar.php",
		            type: "get",
		            data:  data,
		            contentType: false,
		            cache: false,
		            processData:false,
		            success: function(data)
		            {                 
		             	$("#actualizar_datos").html("");         		                                  
		             	$("#actualizar_datos").html(data);  	            
		             	$("#modal_actualizar_paciente").modal('show');
					},                                              
		            beforeSend:function()
		            {                  
		             $("#uploadPreview_a").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});
	});

	$(".btn_pagos_paciente").click(function(){		
		
		//verificar acceso por password y cargar variable de session
		var id_paciente_pagos=$(this).attr('id_paciente');		
		$.ajax({
		            url: "../controller/pacientes/pagos/validad_acceso_pagos.php",
		            type: "post",
		            data:  "",		          
		            success: function(response)
		            {             
		             	if(response=="true")
		             	{
		             		window.location.href = "../view/mod_pago_pacientes.php?id_paciente="+id_paciente_pagos;
		             	}else
		             	{

		             		//var id_paciente_pagos=$(this).attr('id_paciente');		
							var codigo= prompt("Inserta Codigo");
							var data_codigo='codigo='+codigo;
							
							//verificar acceso por password y cargar variable de session
							$.ajax({
							            url: "../controller/pacientes/pagos/validad_acceso_pagos.php",
							            type: "post",
							            data:  data_codigo,		          
							            success: function(response)
							            {             
							             	if(response=="true")
							             	{
							             		window.location.href = "../view/mod_pago_pacientes.php?id_paciente="+id_paciente_pagos;
							             	}else
							             	{
							             		alert('Codigo No Valido');
							             	}
										},                                              
							            beforeSend:function()
							            {                  
							             $("#uploadPreview_a").attr('src','../recursos/img/loading.gif');                                       
							            }   
					         });

							//direcionar 

		             	}
					},                                              
		            beforeSend:function()
		            {                  
		             $("#uploadPreview_a").attr('src','../recursos/img/loading.gif');                                       
		            }   
         });
								
	});

	$(".view_info").click(function(){

		var id_paciente=$(this).attr('id_paciente');		
		var data='id_paciente='+id_paciente;

		$.ajax({
		            url: "../controller/pacientes/cargar_datos_paciente.php",
		            type: "get",
		            data:  data,
		            contentType: false,
		            cache: false,
		            processData:false,
		            success: function(data)
		            {                 
		             	$("#view_datos").html("");         		                                  
		             	$("#view_datos").html(data);  	            
		             	$("#modal_datos_paciente").modal('show');
					},                                              
		            beforeSend:function()
		            {                  
		             $("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});

		
	});


	$(".btn_eliminar").click(function(){
		
		var id_paciente=$(this).attr('id_paciente');		
		var data='id_paciente='+id_paciente;


		var r = confirm("¿Está seguro de eliminar el paciente?");
		if (r == true) 
		{
		   $.ajax({
		            url: "../controller/pacientes/eliminar_paciente.php",
		            type: "post",
		            data:  data,		           
		            success: function(data)
		            {                 
		             	//recargar pagina
		             	location.reload();
					},                                              
		            beforeSend:function()
		            {                  
		             $("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});
		} 
					
	});
	


</script>
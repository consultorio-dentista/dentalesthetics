<?php
session_start();

include_once('../../controller/db/mysql.php');
$db=new MySQL();

$id_servicio=$_POST["id_servicio"];
$rason_social=$_POST["rason_social"];


$sql = "SELECT * FROM historial_servicios WHERE servicios_externo_id_servicios_externo = ".$id_servicio." order by id_historial_servicios ASC";


echo '<div id="h_pagos"> <h1>'.$rason_social.'</h1>';
echo '<button type="button" class="btn_pagos btn btn-success" id="historial_servicios" id_servicio="'.$id_servicio.'" rason_social="'.$rason_social.'" ><span class="glyphicon glyphicon-plus"></span></button>';
echo '<br><br>';
echo '<div id="accordion" role="tablist" aria-multiselectable="true">';


$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  $num_rows=$db->num_rows($consulta);
  $count=1;
  	while($resultados = $db->fetch_array($consulta))
  	{ 
      
      if($count!=$num_rows)      
      {
   			echo '<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$resultados["id_historial_servicios"].'" aria-expanded="true" aria-controls="collapseOne">
                              Tratamiento Fecha Inicio : '.$resultados["fecha_inicio"].'
                            </a>
                            <h4 class="titulo_historial" id_historial_servicios="'.$resultados["id_historial_servicios"].'" valor="'.$resultados["titulo"].'"><strong>'.$resultados["titulo"].'</strong></h4>                                                       
                          </h4>

                        
                          
                        </div>
                        <div id="collapse'.$resultados["id_historial_servicios"].'" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                          


             <table class="table" 1>
              <thead>
                <tr>

                  <th>Fecha</th>
                  <th>Paciente</th>
                  <th>Servicio</th>
                  <th>Cantidad</th>
                  <th>Costo U.</th>
                  <th>SubTotal</th>
                  <th>Abono</th>
                  <th></th>   

                </tr>
              </thead>
              <tbody>';


               $sql = "SELECT * FROM registro_servicios_externos 
              WHERE  historial_servicios_id_historial_servicios = ".$resultados ['id_historial_servicios']." 
              AND estatus='a';";
              
           
              $costo_total=0;  
              $total_abono=0;

              $consulta_r = $db->consulta($sql);
              if($db->num_rows($consulta_r)>0)
              {
                  while($resultados_r = $db->fetch_array($consulta_r))
                  { 
                      echo '<tr>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0001" valor="'.$resultados_r["fecha"].'">'.$resultados_r["fecha"].'</td>';

                          if($resultados_r["id_paciente"]==0)
                          {
                            echo '
                          <td class="celda_paciente" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0110" valor="'.$resultados_r["id_paciente"].'">';

                              echo '<div class="div_paciente_reg_externo" id="div_'.$resultados_r["id_registro_servicios_externos"].'" >';

                               

                              echo '</div>';    
                          
                          echo '</td>'; 

                          }else
                          {
                            echo '
                          <td class="celda_paciente" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0110" valor="'.$resultados_r["id_paciente"].'">';

                              echo '<div class="div_paciente_reg_externo" id="div_'.$resultados_r["id_registro_servicios_externos"].'" >';

                                //buscar foto
                                $sql = "SELECT FO.*, PA.* from paciente PA, persona PE, foto FO 
                                where PA.id_paciente=".$resultados_r["id_paciente"]." 
                                and PA.persona_id_persona=PE.id_persona 
                                and PE.foto_id_foto=FO.id_foto";
                                $consulta_foto = $db->consulta($sql);  
                                if($db->num_rows($consulta_foto)>0)
                                {
                                  while($resultados_foto = $db->fetch_array($consulta_foto))
                                  { 
                                    echo '<img class="view_info img-circle" src="../view/pacientes/foto/'.$resultados_foto["url"].'?nocache=1" width="25px" height="25px" id_paciente="'.$resultados_foto["id_paciente"].'" />';
                                  }
                                }  

                              echo '</div>';    
                          
                          echo '</td>';    

                          }
                      
                          if($resultados_r["servicio"]=="*")
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0010" valor=""></td>';
                          }else
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0010" valor="'.$resultados_r["servicio"].'">'.$resultados_r["servicio"].'</td>'; 
                          }

                      echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0011" valor="'.$resultados_r["cantidad"].'">'.$resultados_r["cantidad"].'</td>                      
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0100" valor="'.$resultados_r["costo"].'">$ '.$resultados_r["costo"].'</td>
                          <td>$'.$resultados_r["cantidad"]*$resultados_r["costo"].'</td>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0101" valor="'.$resultados_r["abono"].'">$ '.$resultados_r["abono"].'</td>
                          <td class="borrar_rh" id_registro="'.$resultados_r["id_registro_servicios_externos"].'"></td>
                          
                        </tr>';
                        $costo_total+=$resultados_r["cantidad"]*$resultados_r["costo"];
                        $total_abono+=$resultados_r["abono"];

                  }
              }


               echo '<tr>
                          <td></td>
                          <td> </td>   
                          <td> </td>                        
                          <td></td>
                          <td><b>Total:</b></td>
                          <td>$ '.$costo_total.'</td>
                          <td></td>
                          <td></td>

                          
                          
                      </tr>
                      <tr>
                          <td></td>
                          <td> </td>   
                          <td> </td>                        
                          <td></td>
                          <td><b>Abono:</b></td>
                          <td>$ '.$total_abono.'</td>
                          <td></td>
                          <td></td>

                         
                          
                      </tr>
                      <tr>
                          <td><button type="button" class="btn_nuevo_registro btn btn-warning"  id_historial_servicios="'.$resultados["id_historial_servicios"].'"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></button></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Adeudo:</b></td>';

                           if($costo_total>=$total_abono)
                          {
                            echo '<td>$'.($costo_total-$total_abono).'</td>';
                          }else
                          {
                             echo '<td>$+'.(-1*($costo_total-$total_abono)).'</td>';
                          }
                          
                          
                          
                      echo '<td></td><td></td></tr>';

              

      echo ' </tbody>
            </table>


                        </div>
                      </div>';  

                      $count++;
      }else
      {

        echo '<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$resultados["id_historial_servicios"].'" aria-expanded="true" aria-controls="collapseOne">
                              Tratamiento Fecha Inicio : '.$resultados["fecha_inicio"].'
                            </a>
                            <h4 class="titulo_historial" id_historial_servicios="'.$resultados["id_historial_servicios"].'" valor="'.$resultados["titulo"].'"><strong>'.$resultados["titulo"].'</strong></4>                                                       
                          </h4>

                        
                          
                        </div>
                        <div id="collapse'.$resultados["id_historial_servicios"].'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          


             <table class="table" 1>
              <thead>
                <tr>
                  <th>Fecha</th>
                   <th>Paciente</th>
                  <th>Servicio</th>
                  <th>Cantidad</th>
                  <th>Costo U.</th>
                  <th>SubTotal</th>
                  <th>Abono</th>
                  <th></th>
                                  
                </tr>
              </thead>
              <tbody>';


               $sql = "SELECT * FROM registro_servicios_externos 
              WHERE  historial_servicios_id_historial_servicios = ".$resultados ['id_historial_servicios']." 
              AND estatus='a';";

              
           
              $costo_total=0;  
              $total_abono=0;

              $consulta_r = $db->consulta($sql);
              if($db->num_rows($consulta_r)>0)
              {
                  while($resultados_r = $db->fetch_array($consulta_r))
                  { 
                      echo '<tr>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0001" valor="'.$resultados_r["fecha"].'">'.$resultados_r["fecha"].'</td>';

                           if($resultados_r["id_paciente"]==0)
                          {
                              echo '
                          <td class="celda_paciente" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0110" valor="'.$resultados_r["id_paciente"].'">';

                              echo '<div class="div_paciente_reg_externo" id="div_'.$resultados_r["id_registro_servicios_externos"].'" >';

                               

                              echo '</div>';    
                          
                          echo '</td>'; 

                          }else
                          {
                           
                           echo '
                          <td class="celda_paciente" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0110" valor="'.$resultados_r["id_paciente"].'">';

                              echo '<div class="div_paciente_reg_externo" id="div_'.$resultados_r["id_registro_servicios_externos"].'" >';

                                //buscar foto
                                $sql = "SELECT FO.*, PA.* from paciente PA, persona PE, foto FO 
                                where PA.id_paciente=".$resultados_r["id_paciente"]." 
                                and PA.persona_id_persona=PE.id_persona 
                                and PE.foto_id_foto=FO.id_foto";
                                $consulta_foto = $db->consulta($sql);  
                                if($db->num_rows($consulta_foto)>0)
                                {
                                  while($resultados_foto = $db->fetch_array($consulta_foto))
                                  { 
                                    echo '<img class="view_info img-circle" src="../view/pacientes/foto/'.$resultados_foto["url"].'?nocache=1" width="25px" height="25px" id_paciente="'.$resultados_foto["id_paciente"].'" />';
                                  }
                                }  

                              echo '</div>';    
                          
                          echo '</td>';    

                          }

                          if($resultados_r["servicio"]=="*")
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0010" valor=""></td>';
                          }else
                          {
                           echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0010" valor="'.$resultados_r["servicio"].'">'.$resultados_r["servicio"].'</td>'; 
                          }

                      echo'<td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0011" valor="'.$resultados_r["cantidad"].'">'.$resultados_r["cantidad"].'</td>                      
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0100" valor="'.$resultados_r["costo"].'">$ '.$resultados_r["costo"].'</td>
                          <td>$'.$resultados_r["cantidad"]*$resultados_r["costo"].'</td>
                          <td class="edit_celda" id_registro="'.$resultados_r["id_registro_servicios_externos"].'" tipo="0101" valor="'.$resultados_r["abono"].'">$ '.$resultados_r["abono"].'</td>
                          <td class="borrar_rh" id_registro="'.$resultados_r["id_registro_servicios_externos"].'"></td>
                          
                        </tr>';
                        $costo_total+=$resultados_r["cantidad"]*$resultados_r["costo"];
                        $total_abono+=$resultados_r["abono"];

                  }
              }


               echo '<tr>
                          <td></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Total:</b></td>
                          <td>$ '.$costo_total.'</td>
                          <td></td>
                          <td></td>

                          
                          
                      </tr>
                      <tr>
                          <td></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Abono:</b></td>
                          <td>$ '.$total_abono.'</td>
                          <td></td>
                          <td></td>

                         
                          
                      </tr>
                      <tr>
                          <td><button type="button" class="btn_nuevo_registro btn btn-warning"  id_historial_servicios="'.$resultados["id_historial_servicios"].'"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></button></td>
                          <td> </td>                        
                          <td></td>
                          <td><b>Adeudo:</b></td>';

                           if($costo_total>=$total_abono)
                          {
                            echo '<td>$'.($costo_total-$total_abono).'</td>';
                          }else
                          {
                             echo '<td>$+'.(-1*($costo_total-$total_abono)).'</td>';
                          }
                          
                          
                          
                      echo '<td></td><td></td></tr>';

              

      echo ' </tbody>
            </table>


                        </div>
                      </div>'; 

                       
      }
 	}
}


echo '</div>';
echo '</div>';


?>

<!-- Modal pacientes para seleccionar -->
<div id="lista_pacientes">
  <div class="modal fade" id="modal_pacientes_lista" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Pacientes </h4>
        </div>
        <div class="modal-body">
      
          <form id="form_registro_servisio_externo_paciente">
            <div class="row">                                

                <div class="col-md-12" >
                                                         
                    <div class="form-group">
                            <input id="b_paciente" type="text" class="form-control" placeholder="Paciente">
                            <input id="id_registro_se_edit" name="id_registro_se_edit" type="hidden" value="">
                            <input id="id_paciente_se_edit" name="id_paciente_se_edit" type="hidden" value=" ">
                    </div>
                                                    
                </div>

               
                <div class="col-md-12" id="l_pacientes">                                                                 
                </div> 
               
                 
            </div>  
            <button type="submit" class="btn btn-success" id="agregar_paciente">Agregar</button>
          </form>

        </div>
        <div class="modal-footer" >
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->                    
        </div>
      </div>
      
    </div>
  </div>
</div>



<script type="text/javascript">
$(document).ready(function()
{
var id_paciente=0;

$("#historial_servicios").click(function()
{    
    id_servicio=$("#historial_servicios").attr('id_servicio');   
    rason_social =$("#historial_servicios").attr('rason_social');

     $.ajax({
                    url:"../controller/servicios_externos/nuevo_historial_pagos.php",
                    type:"post",
                    data: "id_servicio="+id_servicio,
                    success: function(data)
                    {
                          alert(data);
                            $.ajax({
                                  url: "../controller/servicios_externos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_servicio="+id_servicio+"&rason_social="+rason_social,
                                  
                                  success: function(respose)
                                  {                 
                                    $("#reporte_servicios").html("");                                               
                                    $("#reporte_servicios").html(respose);               
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                   //$("#reporte_servicios").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                            
                            //alert(data);

                          return false;      
                        //volver a cargar historiales
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...');
                    }                    
                }); 

  });


$(".btn_nuevo_registro").click(function()
{    
    id_historial_servicios=$(this).attr('id_historial_servicios');    
    id_servicio=$("#historial_servicios").attr('id_servicio');   
    rason_social =$("#historial_servicios").attr('rason_social');
    
    $.ajax({
                    url:"../controller/servicios_externos/nuevo_historial_registro_pago.php",
                    type:"post",
                    data: "id_historial_servicios="+id_historial_servicios,
                    success: function(data)
                    {
                          
                            $.ajax({
                                 url: "../controller/servicios_externos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_servicio="+id_servicio+"&rason_social="+rason_social,
                                  
                                  success: function(data)
                                  {                 

                                    $("#reporte_servicios").html("");                                               
                                    $("#reporte_servicios").html(data);               
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                    //$("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif'); 

                                  }   
                                });
                            
                            

                          return false;      
                        //volver a cargar historiales
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }                    
                }); 

  });


  $(".titulo_historial").click(function () 
  {                 

                    var id_historial_servicios=$(this).attr('id_historial_servicios');                    
                    var OriginalContent = $(this).attr('valor');
                    
                    $(this).addClass("cellEditing");
                    $(this).html("<input style='width:250px;' type='text' value='"+OriginalContent +"'/>");
                                       
                                       
                    $(this).children().first().keypress(function (e) 
                    {                      
                        if (e.which == 13) 
                        {
                            var newContent = $(this).val();
                            
                            $(this).parent().attr('cantidad',newContent);                            
                            $(this).parent().text(newContent);
                            $(this).parent().removeClass("cellEditing");
                            
                            //guardra cambios 
                            $.ajax({
                            type: 'post',
                            url:"../controller/servicios_externos/edit_titulo_historial.php",
                            data:  'id_registro='+id_historial_servicios+"&valor="+newContent,
                            success: function(content)
                                {
                                    //alert(content);
                                      
                                }
                            });
                            return false;  
                        }
                       
                    });

                 
                                        
                    $(this).children().first().blur(function(){
                        $(this).parent().text(OriginalContent);
                        $(this).parent().removeClass("cellEditing");  
                       
                    });
                    
                    $(this).children().first().focus();

                                        
  });








  $(".edit_celda").click(function () 
  {                 
                    var id_registro=$(this).attr('id_registro');
                    var tipo=$(this).attr('tipo');
                    var OriginalContent = $(this).attr('valor');
                    
                    $(this).addClass("cellEditing");
                    $(this).html("<input style='width:100px;' type='text' value='"+OriginalContent +"'/>");
                                       
                                       
                    $(this).children().first().keypress(function (e) 
                    {                      
                        if (e.which == 13) 
                        {
                            var newContent = $(this).val();
                            
                            $(this).parent().attr('cantidad',newContent);                            
                            $(this).parent().text(newContent);
                            $(this).parent().removeClass("cellEditing");
                            
                            //guardra cambios                             
                            $.ajax({
                            type: 'post',
                            url:"../controller/servicios_externos/edit_celda_historial.php",
                            data:  'tipo='+tipo+'&id_registro='+id_registro+"&valor="+newContent,
                            success: function(content)
                                {
                                    //alert(content);
                                   id_servicio=$("#historial_servicios").attr('id_servicio');   
                                   rason_social =$("#historial_servicios").attr('rason_social');
                                     $.ajax({
                                   url: "../controller/servicios_externos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_servicio="+id_servicio+"&rason_social="+rason_social,
                                  
                                  success: function(data)
                                  {                 
                                    
                                    $("#reporte_servicios").html("");                                               
                                    $("#reporte_servicios").html(data);               
                                                  
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                  //$("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                                }
                            });
                            return false;  
                        }
                       
                    });

                 
                                        
                    $(this).children().first().blur(function(){
                        $(this).parent().text(OriginalContent);
                        $(this).parent().removeClass("cellEditing");  
                       
                    });
                    
                    $(this).children().first().focus();

                                        
  });


 $('.borrar_rh').dblclick(function(){

    var r = confirm("¿Está seguro de eliminar?");
    if (r == true) 
    {
    var id_registro=$(this).attr("id_registro");
    

          $.ajax({
                    url:"../controller/servicios_externos/eliminar_registro_historial_servicios.php",
                    type:"post",
                    data: "id_registro="+id_registro,
                    success: function(data)
                    {
                                  id_servicio=$("#historial_servicios").attr('id_servicio');   
                                   rason_social =$("#historial_servicios").attr('rason_social');
                                     $.ajax({
                                   url: "../controller/servicios_externos/historial_pagos_tabla.php",
                                  type: "POST",
                                  data:  "id_servicio="+id_servicio+"&rason_social="+rason_social,
                                  
                                  success: function(data)
                                  {                 
                                    
                                    $("#reporte_servicios").html("");                                               
                                    $("#reporte_servicios").html(data);               
                                    
                                    
                                  },                                              
                                  beforeSend:function()
                                  {                                   
                                   //$("#reporte_pagos").attr('src','../../../../recursos/img/loading.gif');                                       
                                  }   
                                });
                            
                            //alert(data);

                          return false;      
                        //volver a cargar historiales
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }                    
                }); 
    }

 });


 //funcion  cargar pop ip lista de usuario y buscar b
 var dbclick=false;
$(".celda_paciente").click(function(){
  var img_paciente=$(this).find("img");
          setTimeout(function(){
              //singleclick functionality should start here.
              
          if(dbclick ==false)
          {
               //alert("Insertar nombre del paciente directo en la celda");//  dont use alerts in ur code.                          
              var id_paciente=img_paciente.attr("id_paciente");              

               $.ajax({
                    type: 'post',
                    url: '../controller/servicios_externos/cargar_datos_paciente.php',
                    data: 'id_paciente='+id_paciente,
                    success: function(html)
                    {             
                        $("#message").html(html);
                    },
                    beforeSend:function()
                   {                    
                   
                   }
                  });
          }

          },200);
   
}).dblclick(function(){
          dbclick = true;
          //alert("Mostar modalr para buscar paciente");

           $("#id_registro_se_edit").val($(this).attr("id_registro"));
          //alert($("#id_registro_se_edit").val());
           $("#modal_pacientes_lista").modal('show');  
         

          setTimeout(function(){
          dbclick = false;
          },300)
});

/*$(".celda_paciente").dblclick(function(){
         
         
          
});

$(".celda_paciente").click(function(){
          
         
                  
});*/

/*
Buscar pacientes, introduciendo text en b_paciente
*/

  $("#b_paciente").keyup(function()
          { 


            $.ajax({
                type: 'post',
                url: '../controller/servicios_externos/buscar_paciente_citas.php',
                data: 'nombre='+$(this).val(),
                success: function(html)
                {             
                $("#l_pacientes").html(html);
                },
                beforeSend:function()
               {    
                
                $("#l_pacientes").html('');              
                $("#l_pacientes").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...');
               }
              });
          });

  /*
  Submit registar paciente con servicio externo asignado
  */
  $("#form_registro_servisio_externo_paciente").submit(function(e)
  {    

    var url="../controller/servicios_externos/agregar_paciente_servicio_externo.php";
    $.ajax({
      type:"POST",
      url: url,
      data: $(this).serialize(),
      success: function(data)
      {
          //alert(data)
          //actualizar paciente en regsitro de servidio externo
          //var id="#div_"+data;
          //$(id).html("");
          getPaciente_servicio_externo(data);
      }
    });
    e.preventDefault();
  });

  function getPaciente_servicio_externo(id_registro_se)
  {
       
       var url="../controller/servicios_externos/paciente_servicio_externo.php";
        $.ajax({
          type:"POST",
          url: url,
          data: "id_registro_se="+id_registro_se,
          success: function(data)
          {
              //alert(data)
              //actualizar paciente en regsitro de servidio externo
              var id="#div_"+id_registro_se;
              $(id).html("");
              $(id).html(data);
              $("#modal_pacientes_lista").modal('hide');
              
          }
        });        
        false;
  }



});

</script>

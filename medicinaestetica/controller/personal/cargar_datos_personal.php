<?php
session_start();
$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000001010";
include("../permisos/validar_permiso.php");

if($permiso==1)
{

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}

include_once('../db/mysql.php');
$db=new MySQL();


$id_personal= $_GET['id_personal'];

$sql="select PER.*, PE.*, FT.* , D.*, PU.*
from personal PER, persona PE, foto FT ,direccion D, puesto PU
where PER.id_personal=".$id_personal." 
and PER.puesto_id_puesto=PU.id_puesto
and PER.persona_id_persona=PE.id_persona 
and PE.foto_id_foto=FT.id_foto 
and PE.direccion_id_direccion=D.id_direccion";




//datos paciente
$nombre;
$ap_paterno;
$ap_materno;
$edad;
$rfc;

$estado;
$ciudad;
$colonia;
$calle;
$n_calle;
$cp;


$telefono_m;
$telefono_p;

$foto;

$puesto;

//Buscar datos del paciente
$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
    while($resultados = $db->fetch_array($consulta))
    { 
        $foto= $resultados["url"]; 

        $nombre = $resultados["nombre"];  
        $ap_paterno = $resultados["ap_paterno"];
        $ap_materno = $resultados["ap_materno"];
        $edad = $resultados["edad"];
        $rfc =  $resultados["rfc"];

        $estado = $resultados["estado"];
        $ciudad = $resultados["ciudad"];
        $colonia = $resultados["colonia"];
        $calle = $resultados["calle"];
        $n_calle = $resultados["numero"];        
        $cp=$resultados["cp"];
        $puesto=$resultados["puesto"];

  }
}


//buscar sus telefonos
$sql="SELECT * 
  FROM telefono_has_personal THP, telefono T, tipo_telefono TT
  WHERE THP.personal_id_personal = ".$id_personal."
  AND THP.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=1";

  

  $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_m=$resultados["telefono"];
      }
  }

  $sql="SELECT * 
  FROM telefono_has_personal THP, telefono T, tipo_telefono TT
  WHERE THP.personal_id_personal = ".$id_personal."
  AND THP.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=2";

    $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_p=$resultados["telefono"];
      }
  }

}else
{
    echo '<script type="text/javascript">
            alert("Error: Sin Permiso Activado ");
            </script>';     
}

?>


<div class="modal fade" id="modal_datos_paciente" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos <?php echo $puesto;?></h4>
        </div>
        <div class="modal-body">
      
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto" class="contenedor col-md-12" >                              
                      <img class="view_info img-circle" src="../view/personal/foto/<?php echo $foto;?>?nocache=1" width="180" height="180" />
                  </div>                                
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <span class="help-block">Nombre(s): <?php echo $nombre;?></span>
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <span class="help-block">Ap Paterno: <?php echo $ap_paterno;?></span>
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <span class="help-block">Ap materno: <?php echo $ap_materno;?></span>
                      </div> 
                  </div>     

                  <div class="col-md-4 col-xs-12">                    
                        <span class="help-block">Edad: <?php echo $edad;?></span>
                  </div>

                  <div class="col-md-12 col-xs-12">    
                        <div class="form-group">   
                        <span class="help-block">RFC: <?php echo $rfc;?></span>                                                    
                      </div>
                  </div>

              </div> 

            </div>

           <br>

            <div class="accordion" id="accordion2">

              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne_view">
                      Dirección
                    </a>
                  </div>
                  <div id="collapseOne_view" class="accordion-body collapse out">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <span class="help-block">Estado: <?php echo $estado;?></span>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <span class="help-block">Ciudad: <?php echo $ciudad;?></span>
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <span class="help-block">Colonia: <?php echo $colonia;?></span>
                          </div>
                        </div>
                    
                        <div class="col-md-8">    
                            <div class="form-group">                                   
                              <span class="help-block">Calle: <?php echo $calle;?></span>
                            </div> 
                        </div>     

                        <div class="col-md-4">     
                                <div class="form-group">                
                                <span class="help-block">#: <?php echo $n_calle;?></span>
                              </div>
                        </div>

                        <div class="col-md-8">   
                                <div class="form-group">     
                                <span class="help-block">CP: <?php echo $cp;?></span>                                             
                                </div>
                        </div>

                    </div>

                    </div>
                  </div>
              </div>


              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo_view">
                      Telefono
                    </a>
                  </div>
                  <div id="collapseTwo_view" class="accordion-body collapse out">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        
                         <div class="col-md-12">    
                            <div class="form-group">                                   
                              <span class="help-block">Telefono M: <?php echo $telefono_m;?></span>
                            </div> 
                        </div>     

                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <span class="help-block">Telefono P: <?php echo $telefono_p;?></span>
                            </div> 
                        </div>   

                      </div>

                    </div>
                  </div>
              </div>
            

            </div>

            <br>
                      
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <!--<button type="submit" class="btn btn-Primary">Registrar</button>-->
              </div>
            </div>
      

        </div>
        <div class="modal-footer" id="rs_registro_view">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
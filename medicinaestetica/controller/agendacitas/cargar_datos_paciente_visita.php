<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}

include_once('../db/mysql.php');
$db=new MySQL();


$id_paciente= $_GET['id_paciente'];
$id_citas=$_GET['id_citas'];


$sql="SELECT * FROM citas WHERE id_citas = ".$id_citas;


//datos paciente
$nombre;
$ap_paterno;
$ap_materno;

$asunto;
//Buscar datos del paciente
$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
    while($resultados = $db->fetch_array($consulta))
    { 
       
        $nombre = $resultados["nombre_c"];  
        $ap_paterno = $resultados["ap_paterno_c"];
        $ap_materno = $resultados["ap_materno_c"];
        $asunto= $resultados["asunto"];

  }
}






?>


<div class="modal fade" id="modal_datos_paciente" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Paciente Visita</h4>
        </div>
        <div class="modal-body">
      
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto" class="contenedor col-md-12" >                              
                      <img class="view_info img-circle" src="../view/pacientes/foto/user_default.png?nocache=1" width="180" height="180" />
                  </div>                                
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <span class="help-block">Nombre(s): <?php echo $nombre;?></span>
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <span class="help-block">Ap Paterno: <?php echo $ap_paterno;?></span>
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <span class="help-block">Ap materno: <?php echo $ap_materno;?></span>
                      </div> 
                  </div>     
                           
              </div> 

            </div>

            <div class="col-md-12 col-xs-12">    
                        <div class="form-group">   
                        <span class="help-block">Asunto: <?php echo $asunto;?></span>                                                    
                      </div>
            </div>

           <br>
    
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <button id="cancelar_cita" id_cita="<?php echo $id_citas;?>" class="btn btn-warning">Cancelar Cita</button>
              </div>
            </div>
      

        </div>
        <div class="modal-footer" id="rs_registro_view">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>




  <script type="text/javascript">

      $(document).ready(function(){
        $("#cancelar_cita").click(function(){
         

          var data='id_cita='+$(this).attr('id_cita');
           $.ajax({
                                url: "../controller/agendacitas/eliminar_cita.php",
                                type: "get",
                                data:  data,
                                contentType: false,
                                cache: false,
                                processData:false,
                                success: function(data)
                                {                 
                                    $("#rs_registro_view").html("");                                                
                                    $("#rs_registro_view").html(data);                
                                    $('#calendar').fullCalendar('refetchEvents');
                                },                                              
                                beforeSend:function()
                                {                  
                                 $("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       
                                }   
                            });
        });
      });
  </script>
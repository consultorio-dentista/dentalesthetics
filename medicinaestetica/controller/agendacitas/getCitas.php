<?php

// Short-circuit if the client did not give us a date range.
if (!isset($_GET['start']) || !isset($_GET['end'])) 
{
	die("Error Sin seleccicon de fechas.");
}

include_once('../db/mysql.php');
$db=new MySQL();

$sql="SELECT C.*, P.* FROM citas C, paciente PC, persona P 
WHERE C.fecha_inicio >= '".$_GET['start']."' AND C.fecha_fin <= '".$_GET['end']."' 
AND C.paciente_id_paciente=PC.id_paciente 
AND PC.persona_id_persona=P.id_persona";
//echo $sql;

$citas = array(); //creamos un array de citas
$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  	while($resultados = $db->fetch_array($consulta))
  	{ 
  				 $id_citas=$resultados['id_citas'];
  				 $id_paciente=$resultados['paciente_id_paciente'];
  				 
    			 $title=$resultados['nombre']." ".$resultados['ap_paterno']." ".$resultados['ap_materno'];

    			 $start=$resultados['fecha_inicio'];
    			 $hora_start=$resultados['hora_inicio'];;

    			 $end=$resultados['fecha_fin'];;
    			 $hora_end=$resultados['hora_fin'];;

           //si el pasiente tiene el id_paciente == 1 se traata de un pasiente de visita
           if($id_paciente==1)
           {
              $title=$resultados['nombre_c']." ".$resultados['ap_paterno_c']." ".$resultados['ap_materno_c'];
           }

				 $citas[] = array('id_citas'=> $id_citas,
				 				 'id_paciente'=> $id_paciente, 
				 				 'title'=>$title,
				 				 'start'=>$start."T".$hora_start."-05:00",
				 				 'end'=>$end."T".$hora_end."-05:00",);

 	}
}

$json_string = json_encode($citas);
echo $json_string;

?>
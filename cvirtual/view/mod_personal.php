<?php
session_start();

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Cvirtual</title>
    <link rel="shortcut icon" type="image/x-icon" href="../recursos/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css"/>     
    <script language="javascript" src="../recursos/js/jquery-1.9.1.js"></script>  
    <script language="javascript" src="../recursos/js/bootstrap.js"></script>  
    <script language="javascript" src="../recursos/js/jquery.validate.min.js"></script>    
         
     <script>
	    $( document ).ready(function() 
	    {
    		$("#actualizar_user").click(function(){
                $("#modal_personal_actualizar").modal('show');
            });

         

        $("#actualizar_admin_form").validate({
        rules: {
            nombre_u: {
                minlength: 2,
                required: true                
            },
            ap_paterno_u: {
                minlength: 2,
                required: true
            },
            ap_materno_u: {
                minlength: 2,
                required: true
            },
            usuario_u: 
            {
                minlength: 5,
                required: true
            },
            password_u: 
            {
                minlength: 5,
                required: true
            },
            password_u_c: 
            {
                minlength: 5,
                required: true
            }            
        },
        messages: {
                    nombre_u: 
                    {
                        required: "Nombre Requerido.",
                        minlength: "Nombre Mayor a 2 Caracteres."
                    },
                    ap_paterno_u: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    ap_materno_u: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    }
                    ,
                    usuario_u: 
                    {
                        required: "Usuario Requerido.",
                        minlength: "Mayor a 5 Caracteres."  
                    },
                    password_u: 
                    {
                         required: "Password Requerido.",
                         minlength: "Mayor a 5 Caracteres."  
                    },
                    password_u_c: 
                    {
                          required: "Password Confirmación Requerido.",
                          minlength: "Mayor a 5 Caracteres."  
                    }
                  },
        highlight: function (element) 
        {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) 
        {
            element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) 
        {       
            var password=$("#password_u").val();
            var password_c=$("#password_u_c").val();
            
            if(password==password_c)
            {

                $.ajax({
                    url:"../controller/usuario/actualizar_usuario.php",
                    type:'post',
                    data:$("#actualizar_admin_form").serialize(),
                    success: function(data)
                    {
                        $("#rs_registro_actualizar_user").html(data);
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_actualizar_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }
                }); 

            }else
            {
                    alert("Verifica Tu Password. No son iguales.");
            }
        }
    });
    		
		});
     </script>    
     
     <style>
	.navbar-login
	{
	    width: 305px;
	    padding: 10px;
	    padding-bottom: 0px;
	}
	
	.navbar-login-session
	{
	    padding: 10px;
	    padding-bottom: 0px;
	    padding-top: 0px;
	}
	
	.icon-size
	{
	    font-size: 87px;
	}
	
    #contenido
    {
        padding-top: 150px;
    }

    .navbar 
    {
        background-color: #3fb618;
    }

	
	</style>
  </head>

  <body>
  
  <?php include("componente/menu.php"); ?>

<div id="message">
</div>
  

<div id="contenido" >
	<?php include('personal/lista_personal.php');?>
</div>

<!-- Modal -->
  <div class="modal fade" id="modal_personal_actualizar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Admin </h4>
        </div>
        <div class="modal-body">

          <form id="actualizar_admin_form" class="form-horizontal" role="form" method="post"> 
          <input name="id_persona" type="hidden" value="<?php echo $_SESSION['id_persona'];?>">                        
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="nombre_u" type="text" class="form-control" id="nombre_u" placeholder="Nombre(s):" value="<?php echo $_SESSION['nombre_u'];?>">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_paterno_u" type="text" class="form-control" id="ap_paterno_u" placeholder="Ap Paterno:" value="<?php echo $_SESSION['ap_paterno_u'];?>">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_materno_u" type="text" class="form-control" id="ap_materno_u" placeholder="Ap Materno:" value="<?php echo $_SESSION['ap_materno_u'];?>">                  
                          </div>
                        </div>
                    
                        <div class="col-md-7">    
                            <div class="form-group">                                   
                              <input name="usuario_u" type="text" class="form-control" id="usuario_u" placeholder="Usuario:" value="<?php echo $_SESSION['usuario'];?>">                    
                            </div> 
                        </div>     

                        

                        <div class="col-md-7">  
                                <div class="form-group">                   
                                    <input name="password_u" type="password" class="form-control" id="password_u" placeholder="Password">                    
                                </div>
                        </div>
                       

                        <div class="col-md-7">                    
                                <div class="form-group"> 
                                    <input name="password_u_c" type="password" class="form-control" id="password_u_c" placeholder="Password Confirmar">                    
                                </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-offset-9 col-md-12">
                            <button type="submit" class="btn btn-Primary">Actualizar</button>
                            </div>
                        </div>  
                    </div>
          </form>

                     


        </div>
        <div class="modal-footer" id="rs_registro_actualizar_user">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
  
</body>
</html>

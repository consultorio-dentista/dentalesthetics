<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">     
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            <a  href="#" class="navbar-brand">
            <img src="../recursos/img/logo.jpg" class="img-responsive" />
            </a>
        </div>
        
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">                
                
                 <li><a href="mod_citas.php">Agenda Citas</a></li>                                
                
                 <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pacientes<span class="caret"></span></a>
                    <ul class="dropdown-menu">                        
                        <li><a href="ag_pacientes.php">Lista Pacientes</a></li>
                        <li><a href="ag_pago_pacientes.php">Pagos Pacientes</a></li>
                    </ul>
                 </li>-->  

                 <li><a href="mod_pacientes.php">Lista Pacientes</a></li>
                 <!--<li><a href="ag_pago_pacientes.php">Pagos Pacientes</a></li>-->

                 <li><a href="mod_personal.php">Personal</a></li>

                 <li><a href="mod_servicios_externos.php">Servicios Externos</a></li>
                             
             </ul>
             

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="navbar-login">                            
                                <div class="row">
                                
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span class="glyphicon glyphicon-user icon-size"></span>
                                        </p>
                                    </div>
                                    
                                    <div class="col-lg-8">
                                        <p class="text-left">Usuario: <strong>
                                        <?php
                                        echo $_SESSION['usuario'];
                                        ?>
                                        </strong></p>
                                        <!--<p class="text-left small">correoElectronico@email.com</p>-->
                                        <!--<p class="text-left"><a href="#" class="btn btn-primary btn-block btn-sm" id="actualizar_user">Actualizar Datos</a>-->
                                        <a href="mod_config_sys.php"><span class="glyphicon glyphicon-cog"></span></a>
                                        </p>
                                    </div>
                                    
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="../controller/login/logout.php" class="btn btn-danger btn-block">Cerrar Sesion</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>  
            
    </div>

</div>
<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
/** Error reporting */
$id_paciente=$_GET['id_paciente'];
$id_historial_pagos_rehabilitacion=$_GET['id_historial_pagos_rehabilitacion'];


include_once('../../db/mysql.php');
$db=new MySQL();

//buscar los datos 

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../recursos/Classes/PHPExcel.php';



// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties

$objPHPExcel->getProperties()->setCreator("www.odontovirtual.com")
							 ->setLastModifiedBy("www.odontovirtual.com")
							 ->setTitle("Historial de Pagos Paciente xxxxx")
							 ->setSubject("Historial de Pagos")
							 ->setDescription("Arcivo con re gistro de Historial de pagos de paciente.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("pagos tratamiento file");

$sql = "select PE.* from persona PE,paciente PA where PE.id_persona=PA.persona_id_persona and PA.id_paciente=".$id_paciente;
$consulta_dpa=$db->consulta($sql);
$nombre_completo_paciente;
if($db->num_rows($consulta_dpa)>0)
	{		
	  	while($resultados_dpa = $db->fetch_array($consulta_dpa))
	  	{
	  		$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', $resultados_dpa['nombre'])            
            ->setCellValue('B2', $resultados_dpa['ap_paterno'])
            ->setCellValue('C2', $resultados_dpa['ap_materno'])
            ->setCellValue('D2', "")
            ->setCellValue('E2', "")
            ->setCellValue('F2', "");

            $nombre_completo_paciente=$resultados_dpa['nombre'].' '.$resultados_dpa['ap_paterno'].' '.$resultados_dpa['ap_materno'];
	  	}
	}


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A5', 'FECHA')            
            ->setCellValue('B5', 'TRATAMIENTO')
            ->setCellValue('C5', 'CANTIDAD')
            ->setCellValue('D5', 'COSTO UNITARIO')
            ->setCellValue('E5', 'SUBTOTAL')
            ->setCellValue('F5', 'ABONO');

$sql="SELECT * FROM registro_pagos_rehabilitacion WHERE historial_pagos_rehabilitacion_id_historial_pagos_rehabilitacion = ".$id_historial_pagos_rehabilitacion." 
	AND estatus = 'a'";
$consulta=$db->consulta($sql);
if($db->num_rows($consulta)>0)
	{
		$contador=1;
		$total=0;
		$total_abono=0;
	  	while($resultados = $db->fetch_array($consulta))
	  	{ 

	      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.(5+$contador), $resultados["fecha"])            
            ->setCellValue('B'.(5+$contador), $resultados["tratamiento"])
            ->setCellValue('C'.(5+$contador), $resultados["cantidad"])
            ->setCellValue('D'.(5+$contador), $resultados["costo"])
            ->setCellValue('E'.(5+$contador), ($resultados["costo"]*$resultados["cantidad"]))
            ->setCellValue('F'.(5+$contador), $resultados["abono"]);
            $total+=($resultados["costo"]*$resultados["cantidad"]);
            $total_abono+=$resultados["abono"];
            $contador++;
	 	}

	 	 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.(5+$contador), "")            
            ->setCellValue('B'.(5+$contador), "")
            ->setCellValue('C'.(5+$contador), "")
            ->setCellValue('D'.(5+$contador), "TOTAL: ")
            ->setCellValue('E'.(5+$contador), $total)
            ->setCellValue('F'.(5+$contador), "");
        $contador++;    
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.(5+$contador), "")            
            ->setCellValue('B'.(5+$contador), "")
            ->setCellValue('C'.(5+$contador), "")
            ->setCellValue('D'.(5+$contador), "ABONO: ")
            ->setCellValue('E'.(5+$contador), $total_abono)
            ->setCellValue('F'.(5+$contador), "");
        $contador++;    
         $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.(5+$contador), "")            
            ->setCellValue('B'.(5+$contador), "")
            ->setCellValue('C'.(5+$contador), "")
            ->setCellValue('D'.(5+$contador), "ADEUDO: ")
            ->setCellValue('E'.(5+$contador), ($total-$total_abono))
            ->setCellValue('F'.(5+$contador), "");   

	}            

            

// Miscellaneous glyphs, UTF-8
//$objPHPExcel->setActiveSheetIndex(0)
//            ->setCellValue('A4', 'Miscellaneous glyphs')
//            ->setCellValue('A5', 'jnava');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($nombre_completo_paciente);
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_completo_paciente.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
<?php
session_start();
/*$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000001111";
include("../permisos/validar_permiso.php");

if($permiso==1)
{*/

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}
?>

<div class="row" id="controles_camara">  
  <div class="col-md-3 col-xs-0"></div>
  <div class="col-md-3 col-xs-0"></div>
  <div class="col-md-3 col-xs-10"></div>
  <div class="col-md-3 col-xs-2"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_nuevo_usuario"><span class="glyphicon glyphicon-plus"></span></button></div>
</div>

<?php

include_once('../db/mysql.php');
$db=new MySQL();

//cragar lista de usuarios
$sql = "select * from usuario U, persona P where U.estatus='a' and U.persona_id_persona=P.id_persona";
$consulta=$db->consulta($sql);

echo '<div class="table-responsive"> 
		<table class="table">
		    <thead>
		        <tr>
		            <th>Usuario</th>
		            <th>Nombre</th>
		            <th>Apellido Paterno</th>
		            <th>Apellido Materno</th>
		            <th></th>
		        </tr>
		    </thead>

		    <tbody>';


if($db->num_rows($consulta)>0)
	{
	  	while($resultados = $db->fetch_array($consulta))
	  	{ 
	       echo '<tr>
	            <td>'.$resultados["usuario"].'</td>
	            <td>'.$resultados["nombre"].'</td>
	            <td>'.$resultados["ap_paterno"].'</td>
	            <td>'.$resultados["ap_materno"].'</td>
	            <td>	 
	            
	            <button type="button" class="btn_edit btn btn-success" id_usuario="'.$resultados["id_usuario"].'"><span class="glyphicon glyphicon-cog"></span></button>
	            <button type="button" class="btn_permisos btn btn-primary" id_usuario="'.$resultados["id_usuario"].'"><span class="glyphicon glyphicon-th-list"></span></button>
	            <button type="button" class="btn_eliminar btn btn-danger" id_usuario="'.$resultados["id_usuario"].'"><span class="glyphicon glyphicon-minus"></span></button>
	            </td>
	        </tr>';	
	 	}
	}


echo '    </tbody>
	</table>
</div>';

/*}else
{
  echo '<script type="text/javascript">
      alert("Error: Sin Permiso Activado ");
      </script>';   
}*/

?>



<script type="text/javascript">
$(document).ready(function()
{

	$(".btn_eliminar").click(function(){
			var data="id_usuario="+$(this).attr("id_usuario");
			
			var r=confirm("¿Está seguro de eliminar el usuario?");
			if(r==true)
			{	
					$.ajax({
						url: "../controller/sys_confing/usuarios/delect_usuario.php",
						type: "post",
						data: data,
						success: function(data){
							   
                 cargar_lista_usuarios();
                 $("#message").html(data);
            
						},
						beforeSend: function(){
							$("#script-warning").attr('src','../recursos/img/loading.gif');  
						}
					});
			}
	});


  $(".btn_permisos").click(function(){
      var data="id_usuario="+$(this).attr("id_usuario");
      
     
          $.ajax({
            url: "../controller/sys_confing/usuarios/permisos_usuario.php",
            type: "post",
            data: data,
            success: function(data){
              //alert(data);
                 //
                  $("#m_actualizar_permisos").html('');
                  $("#m_actualizar_permisos").html(data);
                 $("#modal_usuario_permisos").modal();
            
            },
            beforeSend: function(){
              $("#script-warning").attr('src','../recursos/img/loading.gif');  
            }
          });      
  });


	$(".btn_edit").click(function()
	{
					var data="id_usuario="+$(this).attr("id_usuario");
		      $.ajax({
    						url: "../controller/sys_confing/usuarios/cargar_usuario.php",
    						type: "post",
    						data: data,
    						success: function(data)
    						{
    							//alert(data);
    							$("#m_actualizar_usuario").html("");
    							$("#m_actualizar_usuario").html(data);
    							$("#modal_usuario_actualizar").modal('show'); 
    						},
    						beforeSend: function(){
    							$("#script-warning").attr('src','../recursos/img/loading.gif');  
    						}
					});
		
	});



    $("#nuevo_admin_form").validate({
        rules: {
            nombre: {
                minlength: 2,
                required: true                
            },
            ap_paterno: {
                minlength: 2,
                required: true
            },
            ap_materno: {
                minlength: 2,
                required: true
            },
            usuario: 
            {
                minlength: 5,
                required: true
            },
            password: 
            {
                minlength: 5,
                required: true
            },
            password_c: 
            {
                minlength: 5,
                required: true
            }            
        },
        messages: {
                    nombre: 
                    {
                        required: "Nombre Requerido.",
                        minlength: "Nombre Mayor a 2 Caracteres."
                    },
                    ap_paterno: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    ap_materno: 
                    {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    }
                    ,
                    usuario: 
                    {
                        required: "Usuario Requerido.",
                        minlength: "Mayor a 5 Caracteres."  
                    },
                    password: 
                    {
                         required: "Password Requerido.",
                         minlength: "Mayor a 5 Caracteres."  
                    },
                    password_c: 
                    {
                          required: "Password Confirmación Requerido.",
                          minlength: "Mayor a 5 Caracteres."  
                    }
                  },
        highlight: function (element) 
        {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) 
        {
            element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) 
        {       
            var password=$("#password").val();
            var password_c=$("#password_c").val();
            
            if(password==password_c)
            {

                $.ajax({
                    url:"../controller/sys_confing/usuarios/nuevo_usuario.php",
                    type:'post',
                    data:$("#nuevo_admin_form").serialize(),
                    success: function(data)
                    {
                        $("#rs_registro_nuevo_user").html(data);                        
                    },
                    beforeSend:function()
                    {
                         $("#rs_registro_nuevo_user").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                    }
                }); 

            }else
            {
                    alert("Verifica Tu Password. No son iguales.");
            }
        }
    });


    $('#modal_nuevo_usuario').on('hidden.bs.modal', function () {
    // do something…
       cargar_lista_usuarios();
    });

    function cargar_lista_usuarios()
    {
                      var data="op=00001";
                     $.ajax({
                                                    url: "../controller/sys_confing/getModulo.php",
                                                    type: "post",
                                                    data:  data,                                    
                                                    success: function(data)
                                                    {                 
                                                        $("#modulo_sys_config").html("");                                                
                                                        $("#modulo_sys_config").html(data);                                                                                                    
                                                    },                                              
                                                    beforeSend:function()
                                                    {                  
                                                       $("#modulo_sys_config").attr('src','../recursos/img/loading.gif');                                                                                
                                                    }   
                                                });

                                    return false;
    }
  
});
</script>


 <div class="modal fade" id="modal_nuevo_usuario" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Nuevo Usuario </h4>
        </div>
        <div class="modal-body">
          <form id="nuevo_admin_form" class="form-horizontal" role="form" method="post">                              
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="nombre" type="text" class="form-control" id="nombre" placeholder="Nombre(s):" value="">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_paterno" type="text" class="form-control" id="ap_paterno" placeholder="Ap Paterno:" value="">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_materno" type="text" class="form-control" id="ap_materno" placeholder="Ap Materno:" value="">                  
                          </div>
                        </div>
                    
                        <div class="col-md-7">    
                            <div class="form-group">                                   
                              <input name="usuario" type="text" class="form-control" id="usuario" placeholder="Usuario:" value="">                    
                            </div> 
                        </div>     

                        

                        <div class="col-md-7">  
                                <div class="form-group">                   
                                    <input name="password" type="password" class="form-control" id="password" placeholder="Password">                    
                                </div>
                        </div>
                       

                        <div class="col-md-7">                    
                                <div class="form-group"> 
                                    <input name="password_c" type="password" class="form-control" id="password_c" placeholder="Password Confirmar">                    
                                </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-offset-9 col-md-12">
                            <button type="submit" class="btn btn-Primary">Agregar</button>
                            </div>
                        </div>  
                    </div>
          </form>                    
        </div>
        <div class="modal-footer" id="rs_registro_nuevo_user">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
</div>


<div id="m_actualizar_usuario">
<!-- Modal -->
  <div class="modal fade" id="modal_usuario_actualizar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Usuario </h4>
        </div>
        <div class="modal-body">

          <form id="actualizar_admin_form" class="form-horizontal" role="form" method="post"> 
          <input name="id_persona" type="hidden" value="<?php echo $_SESSION['id_persona'];?>">                        
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="nombre_u" type="text" class="form-control" id="nombre_u" placeholder="Nombre(s):" value="<?php echo $_SESSION['nombre_u'];?>">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_paterno_u" type="text" class="form-control" id="ap_paterno_u" placeholder="Ap Paterno:" value="<?php echo $_SESSION['ap_paterno_u'];?>">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ap_materno_u" type="text" class="form-control" id="ap_materno_u" placeholder="Ap Materno:" value="<?php echo $_SESSION['ap_materno_u'];?>">                  
                          </div>
                        </div>
                    
                        <div class="col-md-7">    
                            <div class="form-group">                                   
                              <input name="usuario_u" type="text" class="form-control" id="usuario_u" placeholder="Usuario:" value="<?php echo $_SESSION['usuario'];?>">                    
                            </div> 
                        </div>     

                        

                        <div class="col-md-7">  
                                <div class="form-group">                   
                                    <input name="password_u" type="password" class="form-control" id="password_u" placeholder="Password">                    
                                </div>
                        </div>
                       

                        <div class="col-md-7">                    
                                <div class="form-group"> 
                                    <input name="password_u_c" type="password" class="form-control" id="password_u_c" placeholder="Password Confirmar">                    
                                </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-offset-9 col-md-12">
                            <button type="submit" class="btn btn-Primary">Actualizar</button>
                            </div>
                        </div>  
                    </div>
          </form>

                     


        </div>
        <div class="modal-footer" id="rs_registro_actualizar_user">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
</div>


<div id="m_actualizar_permisos">

</div>
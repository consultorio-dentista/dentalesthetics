<?php
session_start();
$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000001110";
include("../permisos/validar_permiso.php");

if($permiso==1)
{

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}

include_once('../db/mysql.php');
$db=new MySQL();


$id_personal= $_GET['id_personal'];


$sql="select PER.*, PE.*, FT.* , D.*, PU.*
from personal PER, persona PE, foto FT ,direccion D, puesto PU
where PER.id_personal=".$id_personal." 
and PER.puesto_id_puesto=PU.id_puesto
and PER.persona_id_persona=PE.id_persona 
and PE.foto_id_foto=FT.id_foto 
and PE.direccion_id_direccion=D.id_direccion";


//datos paciente
$nombre;
$ap_paterno;
$ap_materno;
$edad;
$rfc;

$estado;
$ciudad;
$colonia;
$calle;
$n_calle;
$cp;

$telefono_m;
$telefono_p;

$foto;

$id_puesto;
$puesto;

//Buscar datos del paciente
$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  	while($resultados = $db->fetch_array($consulta))
  	{ 
        $foto= $resultados["url"]; 

   			$nombre = $resultados["nombre"];	
   			$ap_paterno = $resultados["ap_paterno"];
   			$ap_materno = $resultados["ap_materno"];
        $edad = $resultados["edad"];
        $rfc=$resultados["rfc"];

        $estado = $resultados["estado"];
        $ciudad = $resultados["ciudad"];
        $colonia = $resultados["colonia"];
        $calle = $resultados["calle"];
        $n_calle = $resultados["numero"];       
        $cp=$resultados["cp"]; 
        $id_puesto=$resultados["id_puesto"];
        $puesto=$resultados["puesto"];

 	}
}


//buscar sus telefonos
$sql="SELECT * 
  FROM telefono_has_personal THP, telefono T, tipo_telefono TT
  WHERE THP.personal_id_personal = ".$id_personal."
  AND THP.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=1";

  

  $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_m=$resultados["telefono"];
      }
  }

  $sql="SELECT * 
  FROM telefono_has_personal THP, telefono T, tipo_telefono TT
  WHERE THP.personal_id_personal = ".$id_personal."
  AND THP.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=2";

    $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_p=$resultados["telefono"];
      }
  }

}else
{
    echo '<script type="text/javascript">
            alert("Error: Sin Permiso Activado ");
            </script>';     
}

?>



<div class="modal fade" id="modal_actualizar_personal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Actualizar Personal </h4>
        </div>
        <div class="modal-body">

        <!--<form id="image_user_form_file" method="post" enctype="multipart/form-data">-->
            <div id='input_file_img_a' class="col-md-12">
              <input type="file" name="file_img_a" id="file_img_a">              
            </div>
        <!--</form>-->

          <form id="actualizar_form_paciente" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" >
 
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto_a" class="contenedor col-md-12" >         
                      
                      <canvas id="foto_a" ></canvas>
                      <video id="camara_a" autoplay controls></video>
                      <img src="../view/personal/foto/<?php echo $foto;?>?nocache=1" id="uploadPreview_a" />

                  </div>  

                  <div id='btns_control_camara_a' class="col-md-12 col-md-offset-4" >
                    <!--<input id='botonIniciar' type='button' value = 'Iniciar' class="btn btn-success btn-sm"></input>-->
                    <!--<input id='botonDetener' type='button' value = 'Detener' class="btn btn-success btn-sm"></input>-->
                    <span id='botonFoto_a' type='button'  class="btn btn-success btn-sm glyphicon glyphicon-record"></span>
                    <span id='botonFile_a' type='button'  class="btn btn-success btn-sm glyphicon glyphicon-picture"></span>
                  </div>
                                
                  <div class="row">
                    <div class="col-md-6 col-xs-6" >  
                        <!--<input type="radio" name="sex" value="s_camara">-->
                        <span id="s_camara_a" class="glyphicon glyphicon-camera btn btn-Warning XSmall"></span>                      
                    </div> 
                    <div class="col-md-6 col-xs-6">  
                        <!--<input type="radio" name="sex" value="s_file">-->
                        <span id="s_file_a" class="glyphicon glyphicon-picture btn btn-Warning  XSmall"></span>                      
                    </div> 
                  </div>
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <input name="nombre_a" type="text" class="form-control" id="nombre_a" placeholder="Nombre(s)" value="<?php echo $nombre; ?>">                  
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <input name="ap_paterno_a" type="text" class="form-control" id="ap_paterno_a" placeholder="Ap. Paterno"  value="<?php echo $ap_paterno; ?>">                  
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <input name="ap_materno_a" type="text" class="form-control" id="ap_materno_a" placeholder="Ap. Materno"  value="<?php echo $ap_materno; ?>">                    
                      </div> 
                  </div>     

                  <div class="col-md-4 col-xs-12"> 
                        <div class="form-group">                    
                        <input name="edad_a" type="text" class="form-control" id="edad_a" placeholder="Edad" value="<?php echo $edad; ?>">                    
                      </div>
                  </div>

                  <div class="col-md-12 col-xs-12">    
                        <div class="form-group">                 
                        <input name="rfc_a" type="text" class="form-control" id="rfc_a" placeholder="RFC" value="<?php echo $rfc; ?>">                    
                      </div>
                  </div>

                  <div class="col-md-12 col-xs-12">    
                        <div class="form-group">                 
                        <input name="t_personal" type="text" class="form-control" id="t_personal" placeholder="Puesto" value="<?php echo $puesto;?>">                    
                          
                                                                        
                        </div>
                  </div>

              </div> 

            </div>

           <br>

            <div class="accordion" id="accordion2">

              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                      Dirección
                    </a>
                  </div>
                  <div id="collapseOne" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <input name="estado_a" type="text" class="form-control" id="estado_a" placeholder="Estado" value="<?php echo $estado; ?>">                  
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="ciudad_a" type="text" class="form-control" id="ciudad_a" placeholder="Ciudad" value="<?php echo $ciudad; ?>">                  
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <input name="colonia_a" type="text" class="form-control" id="colonia_a" placeholder="Colonia" value="<?php echo $colonia; ?>">                  
                          </div>
                        </div>
                    
                        <div class="col-md-8">    
                            <div class="form-group">                                   
                              <input name="calle_a" type="text" class="form-control" id="calle_a" placeholder="Calle" value="<?php echo $calle; ?>">                    
                            </div> 
                        </div>     

                        <div class="col-md-4">   
                                <div class="form-group">                  
                                <input name="numero_calle_a" type="text" class="form-control" id="numero_calle_a" placeholder="#" value="<?php echo $n_calle; ?>">                    
                              </div>
                        </div>

                        <div class="col-md-8">   
                                <div class="form-group">                  
                                <input name="cp_a" type="text" class="form-control" id="cp_a" placeholder="Codigo Postal" value="<?php echo $cp; ?>">                    
                                </div>
                        </div>

                    </div>

                    </div>
                  </div>
              </div>


              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                      Telefono
                    </a>
                  </div>
                  <div id="collapseTwo" class="accordion-body collapse in">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        
                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <input name="telefono_m_a" type="text" class="form-control" id="telefono_m" placeholder="Tel. Movil" value="<?php echo $telefono_m; ?>">                    
                            </div> 
                        </div>     

                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <input name="telefono_p_a" type="text" class="form-control" id="telefono_p" placeholder="Tel. Particular" value="<?php echo $telefono_p; ?>">                    
                            </div> 
                        </div>     

                      </div>

                    </div>
                  </div>
              </div>              

            </div>

            <br>
                      
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <input type="hidden" value="<?php echo $id_personal; ?>" id="id_personal" name="id_personal">
                <button type="submit" class="btn btn-Primary">Actualizar</button>
              </div>
            </div>

          </form>


        </div>
        <div class="modal-footer" id="rs_registro_a">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>

 <script type="text/javascript">
  $(document).ready(function()
  {
    var upload_foto_base64_a=false;
    var upload_foto_file_a=false;


$("#s_camara_a").click(function()
{
    $("#foto_a").hide();
    $("#btns_control_camara_a").show();
    $("#camara_a").show();


    $(this).hide();
    $("#s_file_a").hide();
    $("#botonFile_a").hide();
    $("#botonFoto_a").show();

    $("#uploadPreview_a").hide();

    navigator.getUserMedia({
        'audio': false,
        'video': true
    }, function(streamVideo) {
        datosVideo.StreamVideo = streamVideo;
        datosVideo.url = window.URL.createObjectURL(streamVideo);
        jQuery('#camara_a').attr('src', datosVideo.url);

    }, function() 
    {
        alert('No fue posible obtener acceso a la cámara.');
    });

});

$("#s_file_a").click(function()
{
    $("#btns_control_camara_a").show();
    $(this).hide();
    $("#s_camara_a").hide();
    $("#botonFoto_a").hide();
    $("#botonFile_a").show();

     $("#uploadPreview_a").show();
     $("#foto_a").hide();

});

window.URL = window.URL || window.webkitURL;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia ||
function() 
{
    alert('Su navegador no soporta navigator.getUserMedia().');
};

//Este objeto guardará algunos datos sobre la cámara
window.datosVideo = 
{
    'StreamVideo': null,
    'url': null
}

/*jQuery('#botonIniciar').on('click', function(e) 
{

    //Pedimos al navegador que nos da acceso a 
    //algún dispositivo de video (la webcam)
    navigator.getUserMedia({
        'audio': false,
        'video': true
    }, function(streamVideo) {
        datosVideo.StreamVideo = streamVideo;
        datosVideo.url = window.URL.createObjectURL(streamVideo);
        jQuery('#camara').attr('src', datosVideo.url);

    }, function() 
    {
        alert('No fue posible obtener acceso a la cámara.');
    });

});*/





/*jQuery('#botonDetener').on('click', function(e) {

    if (datosVideo.StreamVideo) {
        datosVideo.StreamVideo.stop();
        window.URL.revokeObjectURL(datosVideo.url);
    }

});*/

jQuery('#botonFoto_a').on('click', function(e) {
    var oCamara, oFoto, oContexto, w, h;

    oCamara = jQuery('#camara_a');
    oFoto = jQuery('#foto_a');
    w = oCamara.width();
    h = oCamara.height();
    oFoto.attr({
        'width': w,
        'height': h
    });
    oContexto = oFoto[0].getContext('2d');
    oContexto.drawImage(oCamara[0], 0, 0, w, h);    

    $("#camara_a").hide();
    $("#s_camara_a").show();
    $("#s_file_a").show();
    $(this).hide();

     if (datosVideo.StreamVideo) 
     {
        //datosVideo.StreamVideo.stop();
        window.URL.revokeObjectURL(datosVideo.url);
    }

    $("#camara_a").hide();
    $("#foto_a").show();

    upload_foto_base64_a=true;
    upload_foto_file_a=false;
    
    //window.location.href = dataURL;
  });


    jQuery('#botonFile_a').on('click', function(e) 
    {
      var oCamara, oFoto, oContexto, w, h;
      $("#file_img_a").click();        

    });

    $( "#file_img_a" ).change(function(e) 
    {
      
      //alert("Preeview Img");

      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("file_img_a").files[0]);

      oFReader.onload = function (oFREvent) 
        {
            document.getElementById("uploadPreview_a").src = oFREvent.target.result;
        };


      $("#botonFoto_a").hide();
      $("#botonFile_a").hide();

      $("#s_camara_a").show();
      $("#s_file_a").show();


      upload_foto_base64_a=false;
      upload_foto_file_a=true;  

    });


    $("#actualizar_form_paciente").validate({
        rules: {
            nombre: {
                minlength: 2,
                required: true                
            },
            ap_paterno: {
                minlength: 2,
                required: true
            },
            ap_materno: {
                minlength: 2,
                required: true
            }
            ,
            edad: {
                minlength: 1,
                required: true
            },
            estado: {
                minlength: 2,
                required: true                
            },
            ciudad: {
                minlength: 2,
                required: true
            },
            colonia: {
                minlength: 2,
                required: true
            }
            ,
            calle: {
                minlength: 1,
                required: true
            },
            numero_calle: {                
                required: true
            },
            telefono: {                
                required: true
            }
        },
        messages: {
                    nombre: {
                        required: "Nombre Requerido.",
                        minlength: "Nombre Mayor a 2 Caracteres."
                    },
                    ap_paterno: {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    ap_materno: {
                        required: "Apellido Requerido.",
                        minlength: "Apellido Mayor a 2 Caracteres."
                    },
                    edad: {
                        required: "Edad Requerido.",
                        minlength: "Edad Mayor a 0."
                    },
                    estado: {
                        required: "Estado Requerido.",
                        minlength: "Mayor a 2 Caracteres."              
                    },
                    ciudad: {
                        required: "Ciudad Requerido.",
                        minlength: "Mayor a 2 Caracteres."  
                    },
                    colonia: {
                        required: "Colonia Requerido.",
                        minlength: "Mayor a 2 Caracteres."  
                    }
                    ,
                    calle: {
                        required: "Calle Requerido.",
                        minlength: "Mayor a 2 Caracteres."  
                    },
                    numero_calle: {
                        required: "# Requerido."
                    },
                    telefono: {
                         required: "Telefono Requerido."
                    }

                  },
        highlight: function (element) 
        {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) 
        {
            element.text('OK!').addClass('valid').closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) 
        {
            // do other things for a valid form
            //varificar de que fuente se optiene la imagen
                       
                                  
                        $.ajax({
                            type: 'post',
                            url: '../controller/personal/actualizar_personal.php',
                            data: $('#actualizar_form_paciente').serialize(),
                            success: function(html)
                            {   
                                
                                //subir foto  
                                var id_persona_a=html;
                                
                                //alert(id_persona_a);

                                if(upload_foto_base64_a==true)//subir img por String base64img
                                {     

                                          //enviar img de canvas source
                                          var canvas = document.getElementById('foto_a');  
                                          var dataURL = canvas.toDataURL();  
                                        
                                          $.ajax({
                                                  type: 'post',
                                                  url: '../controller/personal/upload_foto_base64img.php',
                                                  data: {foto:dataURL,id_persona:id_persona_a},
                                                  success: function(html)
                                                  {   
                                                      $("#rs_registro_a").html("");  
                                                      //limpiar form 
                                                      //cerrar form
                                                      //$('#modal_nuevo_paciente').modal('hide');  
                                                      upload_foto_base64_a=false;
                                                      upload_foto_file_a=false;
                                                                                                        },
                                                  beforeSend:function()
                                                  {                  
                                                    
                                                  }
                                                });
                                             
                                }else
                                  {
                                    if(upload_foto_file_a==true)//subir 
                                    {                                              
                                              var file_data = $('#file_img_a').prop('files')[0];   
                                              var form_data = new FormData();                  
                                              form_data.append('file_img', file_data);
                                              form_data.append('id_persona', id_persona_a);
                                              
                                              $.ajax({
                                              url: "../controller/personal/upload_foto_file.php",
                                              type: "POST",
                                              data:  form_data,
                                              contentType: false,
                                              cache: false,
                                              processData:false,
                                              success: function(data)
                                              {                                                
                                                $("#rs_registro_a").html("");  
                                                 //$('#modal_nuevo_paciente').modal('hide');  
                                                 upload_foto_base64_a=false;
                                                 upload_foto_file_a=false;
                                                 
                                              },                                              
                                                  beforeSend:function()
                                                  {                  
                                                    
                                                  }   
                                              });
                                              
                                            
                                    }else
                                    {
                                      $("#rs_registro_a").html(""); 
                                    }
                                  }
                            },
                            beforeSend:function()
                            {                  
                                $("#rs_registro_a").html('<img src="../recursos/img/loading.gif" width="30px" height="30px"> Loading...')
                            }
                          });                            
        }
    });

  });
</script>
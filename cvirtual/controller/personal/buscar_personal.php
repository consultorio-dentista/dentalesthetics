<?php
session_start();
$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000001011";
include("../permisos/validar_permiso.php");

if($permiso==1)
{

$nombre=$_POST['nombre'];

include_once('../db/mysql.php');
$db=new MySQL();

$sql="select PER.*, PE.*, FT.* , PU.*
from personal PER, persona PE, foto FT ,puesto PU
where PER.persona_id_persona=PE.id_persona 
and PER.puesto_id_puesto=PU.id_puesto
and PE.foto_id_foto=FT.id_foto 
and PER.estatus='a'
and (PE.nombre LIKE '%".$nombre."%' 
or PE.ap_paterno LIKE '%".$nombre."%' 
or PE.ap_materno LIKE '%".$nombre."%')";


echo '<div class="table-responsive"> 
	<table class="table">
	    <thead>
	        <tr>
	            <th></th>
	            <th>Tipo</th>
	            <th>Nombre</th>
	            <th>Apellido Paterno</th>
	            <th>Apellido Materno</th>
	            <th></th>
	        </tr>
	    </thead>

	    <tbody>';


$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  	while($resultados = $db->fetch_array($consulta))
  	{ 
   			echo '<tr>
	            <td align="center" ><img class="view_info img-circle" src="../view/personal/foto/'.$resultados["url"].'?nocache=1" width="100px" height="100px" id_personal="'.$resultados["id_personal"].'" ></td>';


	            /*switch($resultados["id_puesto"])
	            {
	            	case 1:*/
	            		echo '<td><font color="#3fb618">'.$resultados["puesto"].'</font></td>';
	            	/*break;


	            	case 2:
	            		echo '<td><font color="#0087eb">'.$resultados["puesto"].'</font></td>';
	            	break;


	            	case 3:
	            		echo '<td><font color="#F7D358">'.$resultados["puesto"].'</font></td>';
	            	break;
	            }*/
	            

	        echo '<td>'.$resultados["nombre"].'</td>
	            <td>'.$resultados["ap_paterno"].'</td>
	            <td>'.$resultados["ap_materno"].'</td>
	            <td><button type="button" class="btn_actualizar btn btn-primary" id_personal="'.$resultados["id_personal"].'"><span class="glyphicon glyphicon-cog"></span></button>
	            	<button type="button" class="btn_eliminar btn btn-danger" id_personal="'.$resultados["id_personal"].'"><span class="glyphicon glyphicon-minus"></span></button>
	            </td>
	        </tr>';	
 	}
}


echo '    </tbody>
	</table>
</div>';

}else
{
    echo '<script type="text/javascript">
            alert("Error: Sin Permiso Activado ");
            </script>';     
}

?>

<script type="text/javascript">
	$(".btn_actualizar").click(function(){		
		
		var id_personal_actualizar=$(this).attr('id_personal');		
		var data='id_personal='+id_personal_actualizar;
		
		$.ajax({
		            url: "../controller/personal/cargar_datos_personal_actualizar.php",
		            type: "get",
		            data:  data,
		            contentType: false,
		            cache: false,
		            processData:false,
		            success: function(data)
		            {                 
		             	$("#actualizar_datos").html("");         		                                  
		             	$("#actualizar_datos").html(data);  	            
		             	$("#modal_actualizar_personal").modal('show');
					},                                              
		            beforeSend:function()
		            {                  
		             $("#uploadPreview_a").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});
	});

	$(".btn_eliminar").click(function(){		
		
		var r = confirm("¿Está seguro de eliminar el Personal?");
		if (r == true) 
		{
		var id_personal=$(this).attr('id_personal');		
		var data='id_personal='+id_personal;
		
		$.ajax({
		            url: "../controller/personal/eliminar_personal.php",
		            type: "POST",
		            data:  data,
		            success: function(data)
		            {                 
		            	
		             	$("#message").html("");         		                                  
		             	$("#message").html(data);  	            		             	
					},                                              
		            beforeSend:function()
		            {                  
		             $("#uploadPreview_a").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});
		}
	});

	$(".view_info").click(function(){

		var id_personal=$(this).attr('id_personal');		
		var data='id_personal='+id_personal;

		$.ajax({
		            url: "../controller/personal/cargar_datos_personal.php",
		            type: "get",
		            data:  data,
		            contentType: false,
		            cache: false,
		            processData:false,
		            success: function(data)
		            {                 
		             	$("#view_datos").html("");         		                                  
		             	$("#view_datos").html(data);  	            
		             	$("#modal_datos_paciente").modal('show');
					},                                              
		            beforeSend:function()
		            {                  
		             $("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});

		
	});


</script>
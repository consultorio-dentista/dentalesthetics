<?php
session_start();

/*$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000000100";
include("../permisos/validar_permiso.php");

if($permiso==1)
{*/

if(!$_SESSION['ok_session']=="ok")
{
    //redireccionar a login
    echo ' <script>
        window.location.href = "../../index.html";
        </script>   ';
}

include_once('../db/mysql.php');
$db=new MySQL();


$id_paciente= $_POST['id_paciente'];

$sql="select PA.*, PE.*, FT.* , D.*
from paciente PA, persona PE, foto FT ,direccion D
where PA.id_paciente=".$id_paciente." 
and PA.persona_id_persona=PE.id_persona 
and PE.foto_id_foto=FT.id_foto 
and PE.direccion_id_direccion=D.id_direccion";


//datos paciente
$nombre;
$ap_paterno;
$ap_materno;
$edad;
$rfc;

$estado;
$ciudad;
$colonia;
$calle;
$n_calle;
$cp;


$telefono_m;
$telefono_p;

$foto;

//Buscar datos del paciente
$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
    while($resultados = $db->fetch_array($consulta))
    { 
        $foto= $resultados["url"]; 

        $nombre = $resultados["nombre"];  
        $ap_paterno = $resultados["ap_paterno"];
        $ap_materno = $resultados["ap_materno"];
        $edad = $resultados["edad"];
        $rfc =  $resultados["rfc"];

        $estado = $resultados["estado"];
        $ciudad = $resultados["ciudad"];
        $colonia = $resultados["colonia"];
        $calle = $resultados["calle"];
        $n_calle = $resultados["numero"];        
        $cp=$resultados["cp"];

  }
}


//buscar sus telefonos
$sql="SELECT * 
  FROM paciente_has_telefono PHT, telefono T, tipo_telefono TT
  WHERE PHT.paciente_id_paciente = ".$id_paciente."
  AND PHT.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=1";

  $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_m=$resultados["telefono"];
      }
  }

  $sql="SELECT * 
    FROM paciente_has_telefono PHT, telefono T, tipo_telefono TT
    WHERE PHT.paciente_id_paciente = ".$id_paciente."
    AND PHT.telefono_id_telefono=T.id_telefono 
    AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
    AND TT.id_tipo_telefono=2";

    $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_p=$resultados["telefono"];
      }
  }


  $sql="SELECT * 
  FROM paciente_has_telefono PHT, telefono T, tipo_telefono TT
  WHERE PHT.paciente_id_paciente = ".$id_paciente."
  AND PHT.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=1";




//buscar si tiene tutor asignado

$sql="select P.* , T.* 
from persona P, tutor T
where T.paciente_id_paciente=".$id_paciente." 
and T.persona_id_persona=P.id_persona";



$id_tutor=0;
$nombre_tutor;
$ap_paterno_tutor;
$ap_materno_tutor;

//Buscar datos del tutor
$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
    while($resultados = $db->fetch_array($consulta))
    {        
        $id_tutor = $resultados["id_tutor"];  
        $nombre_tutor = $resultados["nombre"];  
        $ap_paterno_tutor = $resultados["ap_paterno"];
        $ap_materno_tutor = $resultados["ap_materno"];

  }
}

//buscar telefonos de tutor
//buscar sus telefonos
$telefono_m_t;
$telefono_p_t;

if($id_tutor!=0)
{

$sql="SELECT * 
  FROM tutor_has_telefono THT, telefono T, tipo_telefono TT
  WHERE THT.tutor_id_tutor = ".$id_tutor."
  AND THT.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=1";



  $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_m_t=$resultados["telefono"];
      }
  }


$sql="SELECT * 
  FROM tutor_has_telefono THT, telefono T, tipo_telefono TT
  WHERE THT.tutor_id_tutor = ".$id_tutor."
  AND THT.telefono_id_telefono=T.id_telefono 
  AND T.tipo_telefono_id_tipo_telefono=TT.id_tipo_telefono 
  AND TT.id_tipo_telefono=2";

  

  $consulta = $db->consulta($sql);
  if($db->num_rows($consulta)>0)
  {
      while($resultados = $db->fetch_array($consulta))
      { 
          $telefono_p_t=$resultados["telefono"];
      }
  }

}
/*
}else
{
  echo '<script type="text/javascript">
      alert("Error: Sin Permiso Activado ");
      </script>';   
}*/

?>

<script type="text/javascript">
  $(document).ready(function(){
      $("#modal_datos_paciente").modal('show');
  });
</script>>

<div class="modal fade" id="modal_datos_paciente" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Datos Paciente </h4>
        </div>
        <div class="modal-body">
      
            <div class="row">
                          
              <div class="col-md-5 col-xs-12">
                  <div id="div_foto" class="contenedor col-md-12" >                              
                      <img class="view_info img-circle" src="../view/pacientes/foto/<?php echo $foto;?>?nocache=1" width="180" height="180" />
                  </div>                                
              </div> 

              <div class="col-md-7 col-xs-12">
                  <div class="col-md-12 col-xs-12"> 
                    <div class="form-group">                                
                        <span class="help-block">Nombre(s): <?php echo $nombre;?></span>
                    </div>
                  </div>

                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">                                
                        <span class="help-block">Ap Paterno: <?php echo $ap_paterno;?></span>
                    </div>
                  </div>
              
                  <div class="col-md-8 col-xs-12">    
                      <div class="form-group">                                   
                        <span class="help-block">Ap materno: <?php echo $ap_materno;?></span>
                      </div> 
                  </div>     

                  <div class="col-md-4 col-xs-12">                    
                        <span class="help-block">Edad: <?php echo $edad;?></span>
                  </div>

                  <div class="col-md-12 col-xs-12">    
                        <div class="form-group">   
                        <span class="help-block">RFC: <?php echo $rfc;?></span>                                                    
                      </div>
                  </div>

              </div> 

            </div>

           <br>

            <div class="accordion" id="accordion2">

              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne_view">
                      Dirección
                    </a>
                  </div>
                  <div id="collapseOne_view" class="accordion-body collapse out">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">                                
                              <span class="help-block">Estado: <?php echo $estado;?></span>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">                                
                              <span class="help-block">Ciudad: <?php echo $ciudad;?></span>
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">                                
                              <span class="help-block">Colonia: <?php echo $colonia;?></span>
                          </div>
                        </div>
                    
                        <div class="col-md-8">    
                            <div class="form-group">                                   
                              <span class="help-block">Calle: <?php echo $calle;?></span>
                            </div> 
                        </div>     

                        <div class="col-md-4">     
                                <div class="form-group">                
                                <span class="help-block">#: <?php echo $n_calle;?></span>
                              </div>
                        </div>

                        <div class="col-md-8">   
                                <div class="form-group">     
                                <span class="help-block">CP: <?php echo $cp;?></span>                                             
                                </div>
                        </div>

                    </div>

                    </div>
                  </div>
              </div>


              <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo_view">
                      Telefono
                    </a>
                  </div>
                  <div id="collapseTwo_view" class="accordion-body collapse out">
                    <div class="accordion-inner">
                      
                      <div class="row">
                        
                         <div class="col-md-12">    
                            <div class="form-group">                                   
                              <span class="help-block">Teléfono Móvil: <?php echo $telefono_m;?></span>
                            </div> 
                        </div>     

                        <div class="col-md-12">    
                            <div class="form-group">                                   
                              <span class="help-block">Teléfono Particular: <?php echo $telefono_p;?></span>
                            </div> 
                        </div>   

                      </div>

                    </div>
                  </div>
              </div>


               <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree_view">
                      Tutor
                    </a>
                  </div>
                  <div id="collapseThree_view" class="accordion-body collapse">
                    <div class="accordion-inner">
                        
                        <div class="row">
                            <div class="col-md-12"> 
                              <div class="form-group">                                
                                  <span class="help-block">Nombre(s) Tutor: <?php echo $nombre_tutor;?></span>
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">                                
                                  <span class="help-block">Ap paterno Tutor: <?php echo $ap_paterno_tutor;?></span>
                              </div>
                            </div>
                        
                            <div class="col-md-12">    
                                <div class="form-group">                                   
                                  <span class="help-block">Ap materno Tutor: <?php echo $ap_materno_tutor;?></span>
                                </div> 
                            </div>     

                             <div class="col-md-12">    
                                <div class="form-group">                                   
                                  <span class="help-block">Teléfono Móvil: <?php echo $telefono_m_t;?></span>
                                </div> 
                            </div>   

                             <div class="col-md-12">    
                                <div class="form-group">                                   
                                  <span class="help-block">Teléfono Particular: <?php echo $telefono_p_t;?></span>
                                </div> 
                            </div>   

                          </div>
                      </div>

                  </div>
                </div>

            </div>

            <br>
                      
            <div class="form-group"> 
              <div class="col-sm-offset-9 col-md-12">
                <!--<button type="submit" class="btn btn-Primary">Registrar</button>-->
              </div>
            </div>
      

        </div>
        <div class="modal-footer" id="rs_registro_view">
          <!--<button type="button" class="btn btn-Danger" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
<?php
session_start();
$id_usuario=$_SESSION['id_usuario'];
$codigo_permiso="000000010101";
include("../permisos/validar_permiso.php");

if($permiso==1)
{
	

$nombre=$_POST['nombre'];

include_once('../db/mysql.php');
$db=new MySQL();

$sql="select SE.*, PE.*, FT.* 
from servicios_externo SE, persona PE, foto FT 
where SE.persona_id_persona=PE.id_persona 
and PE.foto_id_foto=FT.id_foto 
and SE.estatus='a'
and (PE.nombre LIKE '%".$nombre."%')";


echo '<div class="table-responsive"> 
	<table class="table">
	    <thead>
	        <tr>
	            <th>Logo</th>
	            <th>Razón social</th>
	            <th></th>	           
	            <th></th>
	        </tr>
	    </thead>

	    <tbody>';


$consulta = $db->consulta($sql);
if($db->num_rows($consulta)>0)
{
  	while($resultados = $db->fetch_array($consulta))
  	{ 
   			echo '<tr>
	            <td align="center"><img class="view_info img-circle" src="../view/servicios_externos/foto/'.$resultados["url"].'?nocache=1" width="100px" height="100px" id_servicio_externo="'.$resultados["id_servicios_externo"].'" ></td>

	            <td>'.$resultados["nombre"].'</td>	           

	            <td width="50px">
	            <button type="button" class="btn_pagos_servicios btn btn-warning" id_servicio_externo="'.$resultados["id_servicios_externo"].'" rason_social="'.$resultados["nombre"].'"><span class="glyphicon glyphicon-usd"></span></button>

	            <button type="button" class="btn_actualizar btn btn-primary" id_servicio_externo="'.$resultados["id_servicios_externo"].'"><span class="glyphicon glyphicon-cog"></span></button>

	            <button type="button" class="btn_eliminar btn btn-danger" id_servicio_externo="'.$resultados["id_servicios_externo"].'"><span class="glyphicon glyphicon-minus"></span></button>

	            </td>

	        </tr>';	
 	}
}


echo '    </tbody>
	</table>
</div>';
}else
{
	echo '<script type="text/javascript">
			alert("Error: Sin Permiso Activado ");
			</script>';		
}

?>


<script type="text/javascript">
	

	$(".btn_pagos_servicios").click(function(){				
		

		//verificar acceso por password y cargar variable de session
		var id_servicio=$(this).attr('id_servicio_externo');	
		var rason_social=$(this).attr('rason_social');	

							
							//verificar acceso por password y cargar variable de session
							$.ajax({
							            url: "../controller/servicios_externos/validad_acceso_pagos.php",
							            type: "post",
							            data:  "id_servicio="+id_servicio+"&rason_social="+rason_social,		          
							            success: function(response)
							            {             
							        
												$("#reporte_servicios").html(); 
												$("#reporte_servicios").html(response); 

										},                                              
							            beforeSend:function()
							            {                  
							             //$("#uploadPreview_a").attr('src','../recursos/img/loading.gif');
							             $("#reporte_servicios").html('<center><img src="../recursos/img/loading.gif" width="50px"/></center>');                                       
							            }   
					         });
											

		             			
	});



	var actualzar_b=false;
	var view_b=false;

	$(".btn_actualizar").click(function(){		
		
		if(!actualzar_b)
		{
		actualzar_b=true;	
		var id_servicio_externo=$(this).attr('id_servicio_externo');		
		var data='id_servicio_externo='+id_servicio_externo;
		
		$.ajax({
		            url: "../controller/servicios_externos/cargar_datos_servicio_actualizar.php",
		            type: "post",
		            data:  data,
		            success: function(data)
		            {                 
		             	$("#actualizar_datos").html("");         		                                  
		             	$("#actualizar_datos").html(data);  	            
		             	$("#modal_actualizar_servicio").modal('show');
		             	actualzar_b=false;
					},                                              
		            beforeSend:function()
		            {                  
		             $("#uploadPreview_a").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});
		}
	});

	

	$(".view_info").click(function(){

		if(!view_b)
		{
		view_b=true;	
		var id_servicio_externo=$(this).attr('id_servicio_externo');		
		var data='id_servicio_externo='+id_servicio_externo;


		$.ajax({
		            url: "../controller/servicios_externos/cargar_datos_servicios.php",
		            type: "post",
		            data:  data,
		            success: function(data)
		            {                 
		             	$("#view_datos").html("");         		                                  
		             	$("#view_datos").html(data);  	            
		             	$("#modal_datos_servicio").modal('show');
		             	view_b=false;
					},                                              
		            beforeSend:function()
		            {                  
		             //$("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       
		            }   
          		});
		}
		
	});

	$(".btn_eliminar").click(function(){
		
		var id_servicio_externo=$(this).attr('id_servicio_externo');		
		var data='id_servicio_externo='+id_servicio_externo;


		var r = confirm("¿Está seguro de eliminar el servicio?");
		if (r == true) 
		{
		   $.ajax({
		            url: "../controller/servicios_externos/eliminar_servicio.php",
		            type: "post",
		            data:  data,		           
		            success: function(data)
		            {                 
		             	//recargar pagina
		             	//alert(data);
		             	location.reload();
					},                                              
		            beforeSend:function()
		            {                  
		             //$("#rs_registro_view").attr('src','../recursos/img/loading.gif');                                       		             
		            }   
          		});
		} 
					
	});
	


</script>